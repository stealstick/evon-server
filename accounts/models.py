import os
import random
import time
from datetime import datetime

from django.utils import timezone
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, UserManager
from django.db import models
from django.dispatch import receiver
from chargers.models import Charger

def get_upload_path(instance, filename):
    currently = time.strftime("%y%m%d%H%M%S")
    return os.path.join(
      "profile" , "{}_{}.jpg".format(currently, instance.userid)
    )

def get_serial_number(depth=0):
    serial_number = '10100100'
    for i in range(8):
        serial_number += str(random.randrange(0,9))
    if UserCard.objects.filter(serial_number=serial_number).exists():
        return get_serial_number(depth=depth + 1)
    else:
        return serial_number
class PhoneVaild(models.Model):
    phone_num = models.CharField(max_length = 40, primary_key=True)
    confrim_code = models.CharField(max_length = 10, blank=True)
    confrimed = models.BooleanField(default=False)
    time = models.DateTimeField(default=datetime.now, blank=True)


class User(AbstractBaseUser, PermissionsMixin):
    objects = UserManager()
    userid = models.CharField(max_length = 40, unique=True,default="",blank=True,)
    email = models.CharField(max_length = 100,default="",blank=True, null=True)
    username = models.CharField(max_length=50)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=True)
    date_joined =models.BooleanField(default=True)
    year = models.PositiveIntegerField(default=0, help_text="year")
    phone_number = models.ForeignKey(
        'PhoneVaild', related_name='phone_number', unique=True, null=True, on_delete=models.CASCADE
    )
    kakaotoken= models.CharField(max_length=100,default="",blank=True, null=True)
    
    profile = models.ImageField(upload_to=get_upload_path, default="defalutProfileImg.jpg")
    USERNAME_FIELD = 'userid'
    
    REQUIRED_FIELDS = ['username','year','email']
    
    

    def __str__(self):
        return '%s' % self.username

    def get_short_name(self):
        return self.username

    def save(self, *args, **kwargs):
        super(User, self).save(*args, **kwargs)
        try:
            user_card = getattr(self, "user_card")
        except AttributeError:
            UserCard(user=self).save()


@receiver(models.signals.pre_save, sender=User)
def auto_delete_file_on_change(sender, instance, **kwargs):
    try:
        old_file = User.objects.get(pk=instance.pk).profile
    except User.DoesNotExist:
        return False


    if not instance.pk:
        return False

    try:
        old_file = User.objects.get(pk=instance.pk).profile
    except User.DoesNotExist:
        return False

    new_file = instance.profile
    # if not old_file == new_file:
    #     if os.path.isfile(old_file.path):
    #         os.remove(old_file.path)

class Comaccount(models.Model):

    COM_CHOICES = (
        ('EV', 'evorkr'),
        ('HP', 'happycharger'),
        ('PC', 'Powercube'),
    )
    user = models.ForeignKey(
        'User',
        on_delete=models.CASCADE, related_name='comaccount'
    )
    
    com = models.CharField(max_length=50, choices=COM_CHOICES)
    userid = models.CharField(max_length=50)
    userpw = models.CharField(max_length=50)
    session = models.CharField(max_length=200, default="")
    class Meta:
        unique_together = ('user', 'com',)

class UserCard(models.Model):
    user = models.OneToOneField(User,related_name="user_card", on_delete=models.CASCADE)
    serial_number = models.CharField(max_length=20, default=get_serial_number, unique=True)
    balance = models.BigIntegerField(default=0)
    
class ChargerLike(models.Model):
    user = models.ForeignKey(User,related_name="user_like", on_delete=models.CASCADE)
    charger =   models.ForeignKey(Charger,related_name="charger_like", on_delete=models.CASCADE)
