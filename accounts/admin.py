from django.contrib import admin

from .models import User, Comaccount, PhoneVaild, ChargerLike


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    pass

admin.site.register(Comaccount)
admin.site.register(PhoneVaild)
admin.site.register(ChargerLike)