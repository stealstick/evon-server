import random
import requests
import datetime
import pytz
from datetime import datetime, timedelta
import datetime 
from django.utils import timezone
import random

from django.forms.models import model_to_dict
from rest_framework import status
from rest_framework import viewsets
from rest_framework.authtoken.models import Token
from rest_framework.decorators import detail_route, list_route, api_view
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from accounts.user_card_serializers import UserCardUpdateSerializer
from .models import User, UserCard, Comaccount, PhoneVaild, ChargerLike
from .permissions import IsOwnerOrReadOnly, IsOwnerOrAdmin
from .serializers import (UserSerializer, UserUpdateSerializer,
                          AuthTokenSerializer, UserCardSerializer, ComaccountSerializer)
from urllib.request import urlopen
from chargers.models import Charger
def userprofile(token):
    headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0",
    "Accept-Encoding": "*",
    "Connection": "keep-alive",
    'Authorization': 'Bearer '+token
}
    requestsdata=requests.get('https://kapi.kakao.com/v2/user/me',headers=headers)
    print(requestsdata.text)
    img01=urlopen(requestsdata['properties']['profile_image'])
    print(img01)
    img_file = BytesIO(img01.read())
    resize_img=Image.open(img_file)
    ratio = resize_img.size[0]/720
    saro = resize_img.size[1]/ratio
    size = 720,saro
    resize_img.thumbnail(size)
    buffer = BytesIO()
    resize_img.save(fp=buffer, format='JPEG')
    buff_val = buffer.getvalue()
    user.profile.save('a.jpg', ContentFile(buff_val))
    user.save()


class UserViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.none() 
    parser_classes = (MultiPartParser, FormParser,)
    permission_classes = (IsOwnerOrReadOnly,)

    def update(self, request, pk=None):
        serializer = UserUpdateSerializer(self.request.user,data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

    def create(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            token, created = Token.objects.get_or_create(user=user)
            return Response({'token': token.key})
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)
    def delete(self, request, format=None):
        user=self.request.user
        user.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    
    @list_route(methods=['POST','GET'])
    def login(self, request):
        serializer = AuthTokenSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if "join" in serializer.validated_data:
            user = serializer.save()
            token, created = Token.objects.get_or_create(user=user)
            return Response({'token': token.key})
        
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({'token': token.key})

    @list_route(methods=['GET'])
    def logout(self, request):
        if not request.user.is_authenticated:
            return Response({"message": "Do not exits user"})
        request.user.auth_token.delete()
        return Response({"message": "user token delete success"})

    @list_route(methods=['GET'])
    def me(self, request):
        user = request.user
        serializer = UserSerializer(user)
        return Response(serializer.data)
    
    @list_route(methods=['GET'])
    def findpw(self, request):
        try:
            if request.GET.get('phone') and request.GET.get('email'):
                reset_password= create_reset_password()
                try:
                    phone = PhoneVaild.objects.get(phone_num=request.GET['phone'])
                    user = User.objects.get(phone_number=phone, email=request.GET.get('email'))
                except:
                    return Response({"message": "해당 유저가 없습니다."}, status=status.HTTP_400_BAD_REQUEST)
                user.set_password(reset_password)
                user.save()
                confrim_message= "비밀번호가 ["+reset_password+"]로 초기화 되었습니다."
                headers = {'x-waple-authorization': 'Nzc3OS0xNTIwNDA5NDM4NzM5LTI5NmVlNGQxLWMwYTItNDk5NC1hZWU0LWQxYzBhMjQ5OTQ0Mw=='}
                r = requests.post('http://api.apistore.co.kr/ppurio/1/message/sms/evon', data={'dest_phone':phone.phone_num,'send_phone':'07043530937','id':'evon', 'msg_body':confrim_message},headers=headers)
                return Response({"message": "password reset sucess."})
            else:
                return Response({"message": "이메일과 전화번호를 다시 확인해주세요."}, status=status.HTTP_400_BAD_REQUEST)
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST)
    @list_route(methods=['GET','POST'])
    def findid(self, request):
        if request.method == 'GET':
            if request.GET.get('phone'):
                try:
                    phone = PhoneVaild.objects.get(phone_num=request.GET['phone'])
                    user = User.objects.get(phone_number=phone)
                except:
                    return Response({"message": "해당 유저가 없습니다."}, status=status.HTTP_400_BAD_REQUEST)
                confrim_num=create_confrim_num()
                
                confrim_message='이비온 아이디 찾기 인증번호는 ['+confrim_num+'] 입니다.'
                now = datetime.datetime.now(tz=pytz.timezone('Asia/Seoul'))
                if now-phone.time > timedelta(minutes=1):
                    phone.confrimed = False
                    phone.confrim_code=confrim_num
                    phone.time=datetime.datetime.now(tz=pytz.timezone('Asia/Seoul'))
                    phone.save()
                else:
                    return Response({"message": "Please Wait, you had sent SMS"})
                headers = {'x-waple-authorization': 'Nzc3OS0xNTIwNDA5NDM4NzM5LTI5NmVlNGQxLWMwYTItNDk5NC1hZWU0LWQxYzBhMjQ5OTQ0Mw=='}
                r = requests.post('http://api.apistore.co.kr/ppurio/1/message/sms/evon', data={'dest_phone':phone.phone_num,'send_phone':'07043530937','id':'evon', 'msg_body':confrim_message},headers=headers)

                return Response({"message": "SMS send success"})
            else:
                return Response({"message": "전화번호를 다시 확인해주세요."}, status=status.HTTP_400_BAD_REQUEST)
        
        if request.method == 'POST':
            if request.POST.get('phone') and request.POST.get('code'):
                try:
                    phone = PhoneVaild.objects.get(phone_num=request.POST.get('phone'), confrim_code=request.POST.get('code'))
                    user = User.objects.get(phone_number=phone)
                except:
                    return Response({"message": "코드가 틀렸습니다."}, status=status.HTTP_400_BAD_REQUEST)
                now = datetime.datetime.now(tz=pytz.timezone('Asia/Seoul'))
                if now-phone.time > timedelta(minutes=5):
                    phone.confrimed=False
                    phone.save()
                    return Response({"message": "시간이 경과되었습니다. 다시 인증받으세요."},status=status.HTTP_400_BAD_REQUEST)

                phone.confrimed=True
                phone.save()
                if phone.confrimed:
                    return Response({"message": "등록된 이메일 주소 : "+user.email})
                else:
                    return Response({"message": "Fail"})
                return Response({"message": "Code confrim success"})
            else:
                return Response({"message": "코드가 틀렸습니다."}, status=status.HTTP_400_BAD_REQUEST) 
    






class ComaccountViewSet(viewsets.ModelViewSet):
    serializer_class = ComaccountSerializer
    queryset = Comaccount.objects.filter()
    
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
    def get_queryset(self):
        return Comaccount.objects.filter(user=self.request.user)
    
    def create(self, request):
        context = {
            "request": self.request,
        }
        serializer = ComaccountSerializer(data=request.data, context=context)
        if serializer.is_valid():
            serializer_data = dict(serializer.validated_data)
            #----ev.or.kr----
            if serializer_data['com'] == "EV":
                r = requests.post('http://www.ev.or.kr/portal/mainLogin', data = {'member_id':serializer_data['userid'],'password':serializer_data['userpw']}, allow_redirects=False)
                r_cookie=r.cookies.get_dict()
                print(r_cookie)
                serializer_session=r_cookie['JSESSIONID']
                
                print(r.text)
            #print(serializer_data['userid'])
            #print(serializer_data['userpw'])
                if not 'AlertAndBack' in r.text:
                    session = serializer.save(user=self.request.user, session=serializer_session)
                    return Response(serializer.data)
                
                else:
                    return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)
            #----end ev.or.kr----
            else:
                session = serializer.save(user=self.request.user)
                return Response(serializer.data)

        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

class UserCardViewSet(viewsets.ModelViewSet):
    serializer_class = UserCardSerializer
    queryset = UserCard.objects.all()
    permission_classes = [IsAuthenticated,IsOwnerOrAdmin]

    def update(self, request, pk=None):
        serializer = UserCardUpdateSerializer(UserCard.objects.get(pk=pk), data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)


def create_confrim_num():
    confrim_num = ''
    for i in range(6):
        confrim_num += str(random.randrange(0,9))
    return confrim_num

def create_reset_password():
	return "".join([random.choice("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789") for _ in range(8)])


def vaildate_phone(phone):
    if len(phone) is 11:
        if phone[0] is '0' and phone[1] is '1':
            return True
        else:
            return False
    else:
        return False 

@api_view(['get', 'post'])
def phone_check(request):
    if request.method == 'GET':
        
        try:
            if vaildate_phone(request.GET['phone']):

                confrim_num=create_confrim_num()
                confrim_message='이비온 회원가입 인증번호는 ['+confrim_num+'] 입니다.'
                try:
                    phone = PhoneVaild.objects.create(phone_num=request.GET['phone'], confrim_code=confrim_num )
                except:
                    phone = PhoneVaild.objects.get(phone_num=request.GET['phone'])
                    now = datetime.datetime.now(tz=pytz.timezone('Asia/Seoul'))
                    if now-phone.time > timedelta(minutes=1):
                        phone.confrim_code=confrim_num
                        phone.time=datetime.datetime.now(tz=pytz.timezone('Asia/Seoul'))
                        phone.save() 
                    else:
                        return Response({"message": "Please Wait, you had sent SMS"})

                    phone = PhoneVaild.objects.get(phone_num=request.GET['phone'])
        
                headers = {'x-waple-authorization': 'Nzc3OS0xNTIwNDA5NDM4NzM5LTI5NmVlNGQxLWMwYTItNDk5NC1hZWU0LWQxYzBhMjQ5OTQ0Mw=='}
                r = requests.post('http://api.apistore.co.kr/ppurio/1/message/sms/evon', data={'dest_phone':request.GET['phone'],'send_phone':'07043530937','id':'evon', 'msg_body':confrim_message},headers=headers)
                return Response({"message": "SMS send success"})
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST)
    if request.method == 'POST':
        try:
            if request.POST.get('phone') and request.POST.get('code'):
                phone = PhoneVaild.objects.get(phone_num=request.POST.get('phone'), confrim_code=request.POST.get('code'))
                now = datetime.datetime.now(tz=pytz.timezone('Asia/Seoul'))
                if now-phone.time > timedelta(minutes=5):
                    phone.confrimed=False
                    phone.save()
                    return Response({"message": "시간이 경과되었습니다. 다시 인증받으세요."},status=status.HTTP_400_BAD_REQUEST)

                phone.confrimed=True
                phone.save()
                return Response({"message": "Code confrim success"})
            else:
                return Response({"message": "코드가 틀렸습니다."}, status=status.HTTP_400_BAD_REQUEST)
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST)

@api_view(['get','post','delete'])
def charger_like(request):
    if not request.user.is_authenticated:
        return Response(status=status.HTTP_401_UNAUTHORIZED)
    if request.method == 'GET':
        likes=ChargerLike.objects.filter(user=request.user)
        likes_data={"likes":[]}
        for like in likes:
            likes_data['likes'].append(like.charger.sid)
        return Response( likes_data)
    if request.method == 'POST':
        charger=request.POST.get('sid',None)
        
        if 0<len(Charger.objects.filter(pk=charger)):
            if ChargerLike.objects.filter(user=request.user,charger=Charger.objects.get(sid=charger)):
                return Response({'message': "Like success"})
            like = ChargerLike.objects.create(user=request.user,charger=Charger.objects.get(sid=charger))
            return Response({'message': "Like success"})
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    elif request.method=='DELETE':
        charger=request.POST.get('sid',None)
        if ChargerLike.objects.filter(user=request.user,charger=Charger.objects.get(sid=charger)):
            ChargerLike.objects.filter(user=request.user,charger=Charger.objects.get(sid=charger)).delete()
            return Response({'message': "Unlike success"})
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
            
