from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator
from rest_framework.fields import CurrentUserDefault

from accounts.user_card_serializers import UserCardSerializer
from .models import User, Comaccount, PhoneVaild
#from caveats.serializers import UserFromCaveatManagerSerializer
#from chargerfavorite.serializers import UserForChargerFavoriteSerializer
import re 
import requests
from urllib.request import urlopen
from io import BytesIO

from PIL import Image

from urllib.request import urlopen
from django.core.files.base import ContentFile
def isNumber(s):
  try:
    float(s)
    return True
  except ValueError:
    return False

class ComaccountSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    class Meta:
        model = Comaccount
        
        validators = [
            UniqueTogetherValidator(
                queryset=Comaccount.objects.all(),
                fields=('user', 'com',)
            )
        ]
        fields = ('user','com','userid','userpw','session')
        extra_kwargs = {'userpw': {'write_only': True},
                        'session': {'write_only': True},
                        }

        

class PhonenumberSerializer(serializers.ModelSerializer):

    class Meta:
        model = PhoneVaild
        fields = '__all__'

class UserSerializer(serializers.ModelSerializer):
    #user_card = UserCardSerializer(read_only=True)
    #caveatmanager_set = UserFromCaveatManagerSerializer(read_only=True, many=True)
    charger_favorites = serializers.StringRelatedField(read_only=True, many=True)
    comaccount = ComaccountSerializer(read_only=True, many=True)
    class Meta:
        model = User
        fields = ('pk', 'username', 'userid', 
                  'profile', 'password', 'charger_favorites','comaccount','phone_number','kakaotoken')
        extra_kwargs = {'password': {'required': False,'write_only': True},
                        'profile' : {'required':False},'username' : {'required':False},'userid' : {'required':False},'kakaotoken' : {'required':False},
                        'warn' : {'read_only':True},
                        }

    def validate(self, attrs):
        kakaotoken=attrs.get('kakaotoken')
        if not kakaotoken:
            msg=_('kakaotoken is required')
            raise serializers.ValidationError(msg, code='password')
        headers = {'Authorization': 'Bearer '+kakaotoken}
        requestsdata=requests.get('https://kapi.kakao.com/v2/user/me', headers=headers)
        if requestsdata.status_code !=200:
            msg=_('kakaotoken is validated')
            raise serializers.ValidationError(msg, code='password')

        userdata=requestsdata.json()
        if 0<len(User.objects.filter(userid=userdata['id'])):
            msg=_('Already join')
            raise serializers.ValidationError(msg, code='password')
        userdata['kakaotoken']=kakaotoken
        return userdata

    def create(self, validated_data):
        user=None
        user = User.objects.create(username=validated_data['properties']['nickname'],userid=validated_data['id'], password="", kakaotoken=validated_data['kakaotoken'])
        user.set_password(user.password)
        user.save()
        return user

    


class UserUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('pk', 'username', 'email', 'year', 'profile')
        extra_kwargs = {'username': {'required': False},
                        'email': {'required': False},
                        'profile': {'required': False},
                        'car_type': {'required':False},
                        }

    def update(self, instance, validated_data):
        instance.username = validated_data.get("username", instance.username)
        instance.email = validated_data.get("email", instance.email)
        instance.profile = validated_data.get("profile", instance.profile)
        instance.set_password(validated_data.get('password', instance.password))
        instance.save()
        return instance




class AuthTokenSerializer(serializers.Serializer):
    kakaotoken = serializers.CharField(label=_("kakaotoken"))

    def validate(self, attrs):
        kakaotoken=attrs.get('kakaotoken')
        headers = {'Authorization': 'Bearer '+kakaotoken}
        requestsdata=requests.get('https://kapi.kakao.com/v2/user/me', headers=headers)
        print(requestsdata.text)
        
        if not kakaotoken:
            msg=_('kakaotoken is required')
            raise serializers.ValidationError(msg, code='password')
        if requestsdata.status_code !=200:
            msg=_('kakaotoken is validated')
            raise serializers.ValidationError(msg, code='password')

        userdata=requestsdata.json()
        userdata['kakaotoken']=kakaotoken
        if 0<len(User.objects.filter(userid=userdata['id'])):
            user = User.objects.get(userid=userdata['id'])
            userdata['user']=user
        else:
            userdata['kakaotoken']=kakaotoken
            userdata['join']=True
        return userdata
    def create(self, validated_data):
        user=None
        print(validated_data)
        user = User.objects.create(username=validated_data['properties']['nickname'],userid=validated_data['id'], password="", kakaotoken=validated_data['kakaotoken'])
        user.set_password(user.password)
        user.save()
        img01=urlopen(validated_data['properties']['profile_image'])
        img_file = BytesIO(img01.read())
        resize_img=Image.open(img_file)
        ratio = resize_img.size[0]/720
        saro = resize_img.size[1]/ratio
        size = 720,saro
        resize_img.thumbnail(size)
        buffer = BytesIO()
        resize_img.save(fp=buffer, format='JPEG')
        buff_val = buffer.getvalue()
        user.profile.save('a.jpg', ContentFile(buff_val))


        user.save()
        return user

