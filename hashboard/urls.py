"""hashboard URL Configuration
The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf import settings
from django.conf.urls import include, url
from django.urls import path
from django.conf.urls.static import static
from django.contrib import admin
from rest_framework.routers import DefaultRouter

from accounts.views import UserViewSet, UserCardViewSet, ComaccountViewSet, phone_check, charger_like
from boards.views import BoardViewSet, DataroomViewSet
#from caveats.views import CaveatViewSet
from chargerfavorite.views import ChargerFavoriteViewSet
from chargers.views import ChargerViewSet
from chargers.views import ChargerSearchViewSet, CidStatSearchViewSet, ChargerClearViewSet, ChargerPeakTime, region, Bidlist, evonimg, ChargernameSearchViewSet, imgupload, imgdelete, versioncheck, Ctypelist, bidsvg, KakaoViewSet
from reports.views import ReportViewSet, InquiryViewSet, ChargerReportViewSet
from reviews.views import ChargerReviewViewSet, ReviewLikeViewSet, review_like
router = DefaultRouter()
kakaorouter = DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'comaccount', ComaccountViewSet)
router.register(r'user-cards', UserCardViewSet, base_name="user_card")
router.register(r'chargers', ChargerViewSet)
router.register(r'kakao', KakaoViewSet)
router.register(r'boards', BoardViewSet, base_name="board")
router.register(r'datarooms', DataroomViewSet, base_name="dataroom")
router.register(r'charger_reviews', ChargerReviewViewSet,
                base_name="charger_review")
router.register(r'charger_search', ChargerSearchViewSet,
                base_name="charger_search")
router.register(r'charger_name_search', ChargernameSearchViewSet,
                base_name="charger_search2")
router.register(r'charger_simple', ChargerClearViewSet,base_name="charger_simple")
router.register(r'reports', ReportViewSet, base_name="report")
#router.register(r'caveats', CaveatViewSet, base_name="caveat")
router.register(r'charger_favorites', ChargerFavoriteViewSet,
                base_name="charger_favorite")
router.register(r'inquire', InquiryViewSet,
                base_name="inquire")
router.register(r'charger_report',ChargerReportViewSet, base_name='charger_report')
router.register(r'charger_cidstat', CidStatSearchViewSet, base_name="charger_cidstat")
kakaorouter.register(r'chargers', KakaoViewSet)
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(router.urls)),
    url(r'^api/phonecheck', phone_check),
    url(r'^api/chargers_area/', region),
    url(r'^api/bidlist/',Bidlist),
    url(r'^api/charger_like/',charger_like),
    url(r'^api/review_like/',review_like),
    url(r'^api/ctypelist/',Ctypelist),
    url(r'^api/versioncheck/',versioncheck),
    path('api/chargerpeaktime/', ChargerPeakTime),
    path('marker/<bid>/<stat>/marker.svg', bidsvg),
    url(r'^evonimg/', evonimg),
    url(r'^imgupload/', imgupload),
    url(r'^imgdelete/', imgdelete),
    url(r'^kakao/', include(kakaorouter.urls)),
    path('kakao/chargerpeaktime/', ChargerPeakTime),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
