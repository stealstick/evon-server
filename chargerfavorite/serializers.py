from rest_framework import serializers

from chargers.models import Charger
from .models import ChargerFavorite

"""
class ChargerFavoirteForChargeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Charger
        fields = ('name', 'sid')


class UserForChargerFavoriteSerializer(serializers.ModelSerializer):
    #charger = ChargerForChargerFavoirteSerializer(many=False)

    class Meta:
        model = ChargerFavorite
        fields = ('name', 'charger')

"""
class ChargerFavoriteSerializer(serializers.ModelSerializer):
    #charger = ChargerFavoirteForChargeSerializer(many=False)

    class Meta:
        model = ChargerFavorite
        fields = ('pk', 'user', 'charger')

    def create(self, validated_data):
        charger = validated_data.get("charger")
        user = validated_data.get("user")
        chargerFavorite = ChargerFavorite.objects.create(user=user, charger=charger)
        return chargerFavorite

    def validate(self, attrs):
        charger = attrs.get("charger")
        try:
            print(charger.sid)
            charger = Charger.objects.get(sid=charger.sid)
        except:
            raise serializers.ValidationError(detail='statId doesn\'t exist')
        chargerFavorite = ChargerFavorite.objects.filter(user=attrs["user"], charger=charger)
        if chargerFavorite.count():
            raise serializers.ValidationError(detail='User aleady has this favorite charger')
        attrs["charger"] = charger
        return attrs
