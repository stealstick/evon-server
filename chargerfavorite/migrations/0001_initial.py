# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2018-03-12 06:15
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('chargers', '__first__'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='ChargerFavorite',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('charger', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='charger_favorites', to='chargers.Charger')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='charger_favorites', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
