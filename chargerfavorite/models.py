from django.db import models

from accounts.models import User
from chargers.models import Charger


class ChargerFavorite(models.Model):
    user = models.ForeignKey(User, related_name="charger_favorites", on_delete=models.CASCADE)
    charger = models.ForeignKey(Charger, related_name="charger_favorites", on_delete=models.CASCADE)
    def __str__(self):
        return '%s' % self.charger.sid