from django.contrib import admin

from .models import Report, Inquiry, ChargerReport

admin.site.register(Report)
admin.site.register(Inquiry)
admin.site.register(ChargerReport)