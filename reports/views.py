from rest_framework import viewsets

from .models import Report, Inquiry, ChargerReport
from .serializers import ReportSerializer, InquirySerializer, ChargerReportSerializer


class ReportViewSet(viewsets.ModelViewSet):
    serializer_class = ReportSerializer
    queryset = Report.objects.all()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class InquiryViewSet(viewsets.ModelViewSet):
    serializer_class = InquirySerializer
    queryset = Inquiry.objects.all()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

class ChargerReportViewSet(viewsets.ModelViewSet):
    serializer_class = ChargerReportSerializer
    queryset = ChargerReport.objects.none()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
    