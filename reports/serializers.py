from rest_framework import serializers

from .models import Report, Inquiry, ChargerReport


class ReportSerializer(serializers.ModelSerializer):

    class Meta:
        model = Report
        fields = ('id', 'charger','car','user', 'content', 'piture1', 'piture2', 'piture3', 'status')
        extra_kwargs = {'user': {'required': False},
        'status': {'read_only': True},
                        }


class ChargerReportSerializer(serializers.ModelSerializer):

    class Meta:
        model = ChargerReport
        fields = '__all__'
        extra_kwargs = {'user': {'required': False},
        'status': {'read_only': True},
                        }


class InquirySerializer(serializers.ModelSerializer):
    class Meta:
        model = Inquiry
        fields = ('id', 'user', 'email', 'content', 'status')
        extra_kwargs = {'user': {'required': False},
        'status': {'read_only': True},
                        }