import os
import time

from django.db import models

from accounts.models import User
from chargers.models import Charger

def get_upload_path_01(instance, filename):
    currently = time.strftime("%y%m%d%H%M%S")
    return os.path.join(
      "chargers" , "{}_{}.jpg".format(currently, instance.sid + "_01")
    )

def get_upload_path_02(instance, filename):
    currently = time.strftime("%y%m%d%H%M%S")
    return os.path.join(
      "chargers" , "{}_{}.jpg".format(currently, instance.sid + "_02")
    )
def get_upload_path_03(instance, filename):
    currently = time.strftime("%y%m%d%H%M%S")
    return os.path.join(
      "chargers" , "{}_{}.jpg".format(currently, instance.sid + "_02")
    )

def get_upload_path1(instance, filename):
    currently = time.strftime("%y%m%d%H%M%S")
    return os.path.join(
      "reports", "{}_{}_1.jpg".format(currently, instance.user.username)
    )
def get_upload_path2(instance, filename):
    currently = time.strftime("%y%m%d%H%M%S")
    return os.path.join(
      "reports", "{}_{}_2.jpg".format(currently, instance.user.username)
    )
def get_upload_path3(instance, filename):
    currently = time.strftime("%y%m%d%H%M%S")
    return os.path.join(
      "reports", "{}_{}_3.jpg".format(currently, instance.user.username)
    )


class Report(models.Model):
    STATUS_CHOICE = (
        ("0", "대기 중"),
        ("1", "처리 중"),
        ("2", "처리 완료"),
        ("3", "보류")
    )
    user = models.ForeignKey(User, related_name="reports", on_delete=models.CASCADE)
    charger = models.ForeignKey(Charger, related_name="report_charger",default=0, on_delete=models.CASCADE)
    car = models.CharField(max_length=100, default='0')
    content = models.TextField()
    status = models.CharField(max_length=10, choices=STATUS_CHOICE, default=0)
    piture1 = models.ImageField(blank=True, upload_to=get_upload_path1)
    piture2 = models.ImageField(blank=True, upload_to=get_upload_path2)
    piture3 = models.ImageField(blank=True, upload_to=get_upload_path3)

class ChargerReport(models.Model):
    STATUS_CHOICE = (
        ("0", "대기 중"),
        ("1", "처리 중"),
        ("2", "처리 완료"),
        ("3", "보류")
    )
    user = models.ForeignKey(User, related_name="chargerReport", on_delete=models.CASCADE)
    name = models.CharField(max_length=100, help_text="충전소 이름")
    ctype = models.PositiveIntegerField(default=0, help_text="충전기 타입")
    addrDoro = models.CharField(max_length=100, default="", help_text="도로명")
    detail_addr = models.CharField(max_length=100, default="", help_text="상세주소")
    lat = models.CharField(max_length=100, default=0, help_text="위도")
    lng = models.CharField(max_length=100, default=0, help_text="경도")
    pay = models.CharField(max_length=100, default="", blank=True, help_text="가격")
    tel = models.CharField(max_length=100, default="", help_text="전화번호", blank=True)
    useTime = models.CharField(max_length=100, default=0, help_text="사용 가능 시간")
    charger_img1= models.ImageField(upload_to=get_upload_path_01, default="defalutChargerImg.jpg")
    charger_img2= models.ImageField(upload_to=get_upload_path_02, default="defalutChargerImg.jpg")
    charger_img3= models.ImageField(upload_to=get_upload_path_03, default="defalutChargerImg.jpg")
    note = models.CharField(max_length=200, default=0, help_text="노트")
    status = models.CharField(max_length=10, choices=STATUS_CHOICE, default=0)

class Inquiry(models.Model):
    STATUS_CHOICE = (
        ("0", "대기 중"),
        ("1", "처리 중"),
        ("2", "처리 완료"),
        ("3", "보류")
    )
    user = models.ForeignKey(User, related_name="inquiry", on_delete=models.CASCADE)
    email = models.TextField()
    content = models.TextField()
    status = models.CharField(max_length=10, choices=STATUS_CHOICE, default=0)
