from rest_framework import serializers
from rest_framework.fields import CurrentUserDefault
from django.utils.translation import ugettext_lazy as _

from chargers.serializers import ReviewChargerSerializer
from accounts.models import User
from rest_framework.fields import CurrentUserDefault
from .models import ChargerReview, Like
from chargers.models import Charger

class UserForChargerReviewSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('pk', 'username', 'email',
                  'profile',)

class ChargerReviewSerializer(serializers.ModelSerializer):
    user = UserForChargerReviewSerializer(many=False, read_only=True)

    class Meta:
        model = ChargerReview
        fields = ('id', 'charger', 'user', 'text', 'star')
        extra_kwargs = {
            'user': {'read_only': True},
        }

    def create(self, validated_data):
        validated_data_charger = validated_data.pop("charger")
        charger = Charger.objects.get(sid=self.data['charger'])
        chargerReview = ChargerReview.objects.create(charger=charger,
                                                     **validated_data)
        return chargerReview
class ReviewLikeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Like
        fields = ('review', 'liker')
        extra_kwargs = {'review': {'required': True},
                        'liker': {'required': False},
                        }        
    def create(self, validated_data):
        liker = self.context['request'].user
        like = Like.objects.create(liker=liker,review=validated_data['review'])
        return like
