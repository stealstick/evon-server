from django.contrib import admin
from .models import ChargerReview, Like

admin.site.register(ChargerReview)
admin.site.register(Like)

