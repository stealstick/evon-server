import json

from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import list_route
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from rest_framework.decorators import detail_route, list_route, api_view
from django.http import Http404
from chargers.models import Charger
from .models import ChargerReview, Like
from accounts.models import User
from .serializers import ChargerReviewSerializer, ReviewLikeSerializer

class ChargerReviewViewSet(viewsets.ModelViewSet):
    serializer_class = ChargerReviewSerializer

    def list(self, request, *args, **kwargs):
        sid = request.query_params.get("sid", None)
        if sid is not None:
            charger = Charger.objects.get(sid=sid)
            if not charger:
                return Response({"status": "sid do not exits"},
                                status=status.HTTP_400_BAD_REQUEST)
            reviews = ChargerReview.objects.filter(charger=charger)
            serializer = ChargerReviewSerializer(data=reviews, many=True)
            serializer.is_valid(raise_exception=False)
            datas= serializer.data
            if not request.user.is_active:
                for data in datas:
                    data["owner"]=False
                    data["like"]=False
                return Response(serializer.data)
            for data in datas:

                if data['user']['pk'] == request.user.pk:
                    data["owner"]=True
                else:
                    data["owner"]=False
                if 0 < len(Like.objects.filter(review=ChargerReview.objects.get(pk=data["id"]), liker=User.objects.get(pk=request.user.pk))):
                    data["like"]=True
                else:
                    data["like"]=False
            return Response(serializer.data)
        serializer = ChargerReviewSerializer(data=self.get_queryset(),
                                             many=True)
        serializer.is_valid(raise_exception=False)
        return Response(serializer.data)

    def create(self, request):
        serializer = ChargerReviewSerializer(data=request.data)
        if serializer.is_valid():
            self.perform_create(serializer)
            serializer.data['user']=request.user.username
            response_data=dict(serializer.data)
            response_data['id']=ChargerReview.objects.last().pk
            response_data['user']={
            "pk": request.user.pk,
            "username": request.user.username,
            "email": request.user.email,
            "profile": "/uploads/"+str(request.user.profile)
        }
            response_data["owner"]=False
            response_data["like"]=False
            
            return Response( response_data)
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def get_queryset(self):
        return ChargerReview.objects.all()
    
    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            if instance.user==request.user:
                instance.delete()
            else:
                return Response(status=status.HTTP_401_UNAUTHORIZED)
            
        except Http404:
            pass
        return Response(status=status.HTTP_204_NO_CONTENT)

    @list_route(methods=['POST'])
    def create_review(self, request):
        sid = request.data.get("sid", None)
        user = request.user
        title = request.data.get("title", None)
        star = request.data.get("star", None)
        charger = Charger.objects.filter(sid=sid)[0]
        chargerReview = ChargerReview.objects.create(
                charger=charger, title=title,
                star=star, user=user)
        serializer = ChargerReviewSerializer(chargerReview)
        return Response(serializer.data)

@api_view(['post','delete'])
def review_like(request):
    if not request.user.is_authenticated:
        return Response(status=status.HTTP_401_UNAUTHORIZED)

    if request.method == 'POST':
        review=request.POST.get('review',None)
        if ChargerReview.objects.filter(pk=review):
            like = Like.objects.create(liker=request.user,review=ChargerReview.objects.get(pk=review))
            return Response({'message': "Like success"})
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    elif request.method=='DELETE':
        review=request.POST.get('review',None)
        if Like.objects.filter(liker=request.user,review=ChargerReview.objects.get(pk=review)):
            Like.objects.filter(liker=request.user,review=ChargerReview.objects.get(pk=review)).delete()
            return Response({'message': "Delete success"})
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
            

class ReviewLikeViewSet(viewsets.ModelViewSet):
    serializer_class = ReviewLikeSerializer

    def get_queryset(self):
        return Like.objects.all()

    def create(self, request):
        serializer = ReviewLikeSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            self.perform_create(serializer)
            return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            if instance.liker==request.user:
                instance.delete()
            else:
                return Response(status=status.HTTP_401_UNAUTHORIZED)
            
        except Http404:
            pass
        return Response(status=status.HTTP_204_NO_CONTENT)