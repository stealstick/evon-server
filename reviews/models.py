from django.db import models

from accounts.models import User
from chargers.models import Charger


class ChargerReview(models.Model):
    charger = models.ForeignKey(Charger, on_delete=models.CASCADE,
                                related_name="review")
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                             related_name="charger_review")
    text = models.TextField()
    star = models.PositiveIntegerField(default=1)

    def __str__(self):
        return self.text

class Like(models.Model):
    review = models.ForeignKey(ChargerReview, on_delete=models.CASCADE,
                                related_name="review")
    liker = models.ForeignKey(User, on_delete=models.CASCADE,
                                related_name="review")
    
    
    def __str__(self):
        return str(self.review.charger)+"-"+str(self.review.user)