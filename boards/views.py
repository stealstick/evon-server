from rest_framework import viewsets

from .models import Board, Dataroom
from .serializers import BoardSerializer, DataroomSerializer
from chargers.permissions import IsReadOnly
from rest_framework.pagination import PageNumberPagination
from rest_framework import generics
from rest_framework import filters
from django_filters.rest_framework import DjangoFilterBackend
class StandardResultsSetPagination(PageNumberPagination):
    page_size = 8
    page_size_query_param = 'page_size'

class BoardViewSet(viewsets.ModelViewSet):
    pagination_class = StandardResultsSetPagination
    serializer_class = BoardSerializer
    queryset = Board.objects.all()
    permission_classes = (IsReadOnly,)

    pagination_class = StandardResultsSetPagination
    filter_backends = (DjangoFilterBackend,filters.SearchFilter,filters.OrderingFilter, )
    search_fields = ('title', 'content')
    ordering_fields = '__all__'

class DataroomViewSet(viewsets.ModelViewSet):
    serializer_class = DataroomSerializer
    queryset = Dataroom.objects.all()
    permission_classes = (IsReadOnly,)

    pagination_class = StandardResultsSetPagination
    filter_backends = (DjangoFilterBackend,)
