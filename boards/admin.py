from django.contrib import admin

from .models import Board,Dataroom,Filelink

admin.site.register(Board)
admin.site.register(Dataroom)
admin.site.register(Filelink)

