from rest_framework import permissions
class IsReadOnly(permissions.BasePermission):
    """
    Object-level permission to only allow read-only operations.
    """

    def has_permission(self, request, view):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

class IsKakao(permissions.BasePermission):
    message = 'You do not have permission to perform this action.'
    def has_permission(self, request, view):
        try:
            return request.user.userid=="kakao"
        except:
            return False