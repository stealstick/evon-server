import json

from django.db.models import Q


from io import BytesIO
from PIL import Image
from django.core.files.base import ContentFile
from datetime import datetime, timedelta
from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from django.shortcuts import render
from django.template import Context, Template
from django.template.loader import get_template
from reviews.models import ChargerReview
from .models import Charger, CidStat, Region, Bid, CidstatLog, VersionCheck, Pay, Ctype, Viewtimes
from .serializers import ChargerSerializer
from .serializers import ChargerSimpleSerializer
from .serializers import ChargerSearchSerializer
from .serializers import CidStatSearchSerializer
from .serializers import VersionCheckSerializer
from .serializers import KakaoSerializer
from chargers.permissions import IsReadOnly, IsKakao
from reviews.serializers import ChargerReviewSerializer
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.decorators import list_route, api_view, permission_classes
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from .serializers import ChargerlogSerializer
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django_filters.rest_framework import DjangoFilterBackend
from django_filters import rest_framework as filters
from rest_framework.filters import BaseFilterBackend
class ChargerFilter(filters.FilterSet):
    stlat = filters.NumberFilter(name="lat", lookup_expr='gte')
    enlat = filters.NumberFilter(name="lat", lookup_expr='lte')
    stlng = filters.NumberFilter(name="lng", lookup_expr='gte')
    enlng = filters.NumberFilter(name="lng", lookup_expr='lte')

    class Meta:
        model = Charger
        fields = ['sid','name','ctype','stat', 'pay', 'bid', 'stlat', 'enlat']

class DjangoFilterBackend(BaseFilterBackend):
    filter_template = 'django_filters/rest_framework/crispy_form.html'
    template = filter_template
    
    def to_html(self, request, queryset, view):
        content={
            'ctypes': Ctype.objects.all(),
            'bids': Bid.objects.all(),
            'pays':Pay.objects.all(),
            'stats':[1,2,3,4,5,6,7,8,9]
        }
        for data in request.GET:
            if request.GET.get(data)!="":
                content[data]=dict(request.GET)[data]
            content.pop('stlat', None)
            content.pop('enlat', None)
            content.pop('stlng', None)
            content.pop('enlng', None)
        now=[1,2,3]
        t = get_template('filterform.html')
        
        html = t.render(content)
        return html
class StandardResultsSetPagination(LimitOffsetPagination):
    page_size_query_param = 'page_size'
class ChargerViewSet(viewsets.ModelViewSet):
    serializer_class = ChargerSerializer
    #queryset = Charger.objects.all()
    queryset = Charger.objects.exclude(bid='PW')
    lookup_field = 'pk'
    permission_classes = (IsReadOnly,)
    filter_backends = (DjangoFilterBackend,)
    
    filter_class=ChargerFilter
    def filter_queryset(self, qs):
        filter_query={}
        #------------custom filter-------------
        for data in self.request.GET:
            if self.request.GET.get(data)!="":
                filter_query[data]=dict(self.request.GET)[data]
            filter_query.pop('stlat', None)
            filter_query.pop('enlat', None)
            filter_query.pop('stlng', None)
            filter_query.pop('enlng', None)
        search= self.request.GET.get('search')
        if self.request.GET.get('search') is None:
            search=""
        else:
            qs=qs.filter(name__contains=search)
        if self.request.GET.get('stlat') and  self.request.GET.get('enlat') and self.request.GET.get('stlng') and self.request.GET.get('enlng'):
            xyqueryset = qs.filter(lat__gte=self.request.GET.get('stlat'), lat__lte=self.request.GET.get('enlat'),lng__gte=self.request.GET.get('stlng'), lng__lte=self.request.GET.get('enlng'))
        else:
            xyqueryset = qs
        statqs=qs
        payqs=qs
        bidqs=qs
        ctypeqs=qs
        for i in filter_query:
            if i == 'ctype':
                if "" in filter_query[i]:
                    filter_query[i].remove("")
                if "," in filter_query[i][0]:
                    ctypeqs=qs.filter(ctype__in=filter_query[i][0].split(','))
                else:
                    ctypeqs=qs.filter(ctype__in=filter_query[i])
            if i == 'stat':
                if "" in filter_query[i]:
                    filter_query[i].remove("")
                if "," in filter_query[i][0]:
                    statqs=qs.filter(stat__in=filter_query[i][0].split(','))
                else:
                    statqs=qs.filter(stat__in=filter_query[i])
                
            if i == 'pay':
                if "" in filter_query[i]:
                    filter_query[i].remove("")
                if "," in filter_query[i][0]:
                    payqs=qs.filter(pay__in=filter_query[i][0].split(','))
                else:
                    payqs=qs.filter(pay__in=filter_query[i])
            if i == 'bid':
                if "" in filter_query[i]:
                    filter_query[i].remove("")
                if "," in filter_query[i][0]:
                    bidqs=qs.filter(bid__in=filter_query[i][0].split(','))
                else:
                    bidqs=qs.filter(bid__in=filter_query[i])
        
        if self.request.GET.get('limit') is not None:
            limit=int(self.request.GET.get('limit'))
            offset=0
            if self.request.GET.get('offset') is not None:
                offset=int(self.request.GET.get('offset'))
            return (xyqueryset&statqs&payqs&bidqs&ctypeqs).distinct('sid')[offset:limit+offset]
            
        else:
            return (xyqueryset&statqs&payqs&bidqs&ctypeqs).distinct('sid')
        #------------------------
    @detail_route(methods=['GET'])
    def reviews(self, request, sid=None):
        try:
            charger = Charger.objects.filter(sid=sid)[0]
        except:
            return Response({"status": "fail"},
                            status=status.HTTP_400_BAD_REQUEST)
        chargerReview = ChargerReview.objects.filter(charger=charger)
        return Response(chargerReview)
    
class KakaoViewSet(viewsets.ModelViewSet):
    serializer_class = KakaoSerializer
    #queryset = Charger.objects.all()
    queryset = Charger.objects.exclude(bid='PW')
    lookup_field = 'pk'
    permission_classes = [IsReadOnly, IsKakao]
    filter_backends = (DjangoFilterBackend,)
    
    filter_class=ChargerFilter
    def filter_queryset(self, qs):
        filter_query={}
        #------------custom filter-------------
        for data in self.request.GET:
            if self.request.GET.get(data)!="":
                filter_query[data]=dict(self.request.GET)[data]
            filter_query.pop('stlat', None)
            filter_query.pop('enlat', None)
            filter_query.pop('stlng', None)
            filter_query.pop('enlng', None)
        search= self.request.GET.get('search')
        if self.request.GET.get('search') is None:
            search=""
        else:
            qs=qs.filter(name__contains=search)
        if self.request.GET.get('stlat') and  self.request.GET.get('enlat') and self.request.GET.get('stlng') and self.request.GET.get('enlng'):
            xyqueryset = qs.filter(lat__gte=self.request.GET.get('stlat'), lat__lte=self.request.GET.get('enlat'),lng__gte=self.request.GET.get('stlng'), lng__lte=self.request.GET.get('enlng'))
        else:
            xyqueryset = qs
        statqs=qs
        payqs=qs
        bidqs=qs
        ctypeqs=qs
        for i in filter_query:
            if i == 'ctype':
                if "" in filter_query[i]:
                    filter_query[i].remove("")
                if "," in filter_query[i][0]:
                    ctypeqs=qs.filter(ctype__in=filter_query[i][0].split(','))
                else:
                    ctypeqs=qs.filter(ctype__in=filter_query[i])
            if i == 'stat':
                if "" in filter_query[i]:
                    filter_query[i].remove("")
                if "," in filter_query[i][0]:
                    statqs=qs.filter(stat__in=filter_query[i][0].split(','))
                else:
                    statqs=qs.filter(stat__in=filter_query[i])
                
            if i == 'pay':
                if "" in filter_query[i]:
                    filter_query[i].remove("")
                if "," in filter_query[i][0]:
                    payqs=qs.filter(pay__in=filter_query[i][0].split(','))
                else:
                    payqs=qs.filter(pay__in=filter_query[i])
            if i == 'bid':
                if "" in filter_query[i]:
                    filter_query[i].remove("")
                if "," in filter_query[i][0]:
                    bidqs=qs.filter(bid__in=filter_query[i][0].split(','))
                else:
                    bidqs=qs.filter(bid__in=filter_query[i])
        views=Viewtimes.objects.get(api_url="chargers")
        views.views+=1
        views.save()
        if self.request.GET.get('limit') is not None:
            limit=int(self.request.GET.get('limit'))
            offset=0
            if self.request.GET.get('offset') is not None:
                offset=int(self.request.GET.get('offset'))
            return (xyqueryset&statqs&payqs&bidqs&ctypeqs).distinct('sid')[offset:limit+offset]
            
        else:
            return (xyqueryset&statqs&payqs&bidqs&ctypeqs).distinct('sid')

class ChargerClearViewSet(viewsets.ModelViewSet):
    serializer_class = ChargerSimpleSerializer
    queryset = Charger.objects.exclude(bid='PW')
    permission_classes = (IsReadOnly,)
    filter_backends = (DjangoFilterBackend,)
    filter_class=ChargerFilter
    permission_classes = (IsReadOnly,)
    pagination_class = None
    def filter_queryset(self, qs):
        filter_query={}
        #------------custom filter-------------
        for data in self.request.GET:
            if self.request.GET.get(data)!="":
                filter_query[data]=dict(self.request.GET)[data]
            filter_query.pop('stlat', None)
            filter_query.pop('enlat', None)
            filter_query.pop('stlng', None)
            filter_query.pop('enlng', None)
        search= self.request.GET.get('search')        
        if self.request.GET.get('search') is None:
            search=""
        else:
            qs=qs.filter(name__contains=search)
        if self.request.GET.get('stlat') and  self.request.GET.get('enlat') and self.request.GET.get('stlng') and self.request.GET.get('enlng'):
            xyqueryset = qs.filter(lat__gte=self.request.GET.get('stlat'), lat__lte=self.request.GET.get('enlat'),lng__gte=self.request.GET.get('stlng'), lng__lte=self.request.GET.get('enlng'))
        else:
            xyqueryset = qs

        statqs=qs
        payqs=qs
        bidqs=qs
        ctypeqs=qs
        for i in filter_query:
            if i == 'ctype':
                if "" in filter_query[i]:
                    filter_query[i].remove("")
                if "," in filter_query[i][0]:
                    ctypeqs=qs.filter(ctype__in=filter_query[i][0].split(','))
                else:
                    ctypeqs=qs.filter(ctype__in=filter_query[i])
            if i == 'stat':
                if "" in filter_query[i]:
                    filter_query[i].remove("")
                if "," in filter_query[i][0]:
                    statqs=qs.filter(stat__in=filter_query[i][0].split(','))
                else:
                    statqs=qs.filter(stat__in=filter_query[i])
                
            if i == 'pay':
                if "" in filter_query[i]:
                    filter_query[i].remove("")
                if "," in filter_query[i][0]:
                    payqs=qs.filter(pay__in=filter_query[i][0].split(','))
                else:
                    payqs=qs.filter(pay__in=filter_query[i])
            if i == 'bid':
                if "" in filter_query[i]:
                    filter_query[i].remove("")
                if "," in filter_query[i][0]:
                    bidqs=qs.filter(bid__in=filter_query[i][0].split(','))
                else:
                    bidqs=qs.filter(bid__in=filter_query[i])
        if self.request.GET.get('limit') is not None:
            limit=int(self.request.GET.get('limit'))
            offset=0
            if self.request.GET.get('offset') is not None:
                offset=int(self.request.GET.get('offset'))

            return (xyqueryset&statqs&payqs&bidqs&ctypeqs).distinct('sid')[offset:limit+offset]
            
        else:
            return (xyqueryset&statqs&payqs&bidqs&ctypeqs).distinct('sid')
        #------------------------

from rest_framework import filters
class StandardResultsSetPagination(LimitOffsetPagination):
    page_size_query_param = 'page_size'

class ChargerSearchViewSet(viewsets.ModelViewSet):
    queryset = Charger.objects.all()
    serializer_class = ChargerSearchSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('name', 'addrDoro')
    """
    serializer_class = ChargerSearchSerializer
    def list(self, request, *args, **kwargs):
        search = request.query_params.get("search", None)
        if  search is not None:
            charger = Charger.objects.filter(Q(name__contains= search) | Q(addrDoro__contains = search))
            if not charger:
                return Response({"status": "do not exits"},
                                status=status.HTTP_400_BAD_REQUEST)
            serializer = ChargerSearchSerializer(data=charger, many=True)
            serializer.is_valid(raise_exception=False)
            return Response(serializer.data)
        
        serializer.is_valid(raise_exception=False)
        return Response(serializer.data)
       

    def get_queryset(self):
        return Charger.objects.all()
        """
class ChargernameSearchViewSet(viewsets.ModelViewSet):
    queryset = Charger.objects.order_by('name')
    serializer_class = ChargerSearchSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('name', 'name')
    pagination_class = StandardResultsSetPagination

class CidStatSearchViewSet(viewsets.ModelViewSet):
    serializer_class = CidStatSearchSerializer
    
    def list(self, request, *args, **kwargs):
        sid = request.query_params.get("sid", None)
        if  sid is not None:
            charger = CidStat.objects.filter(charger= sid)
            if not charger:
                return Response({"status": " name do not exits"},
                                status=status.HTTP_400_BAD_REQUEST)
            serializer = CidStatSearchSerializer(data=charger, many=True)
            serializer.is_valid(raise_exception=False)
            return Response(serializer.data)
        serializer = CidStatSearchSerializer(data=self.get_queryset(),
                                             many=True)
        serializer.is_valid(raise_exception=False)
        return Response(serializer.data)
       

    def get_queryset(self):
        return CidStat.objects.all()

@api_view(['get'])
def Bidlist(request):
    if request.method == 'GET':
        bid_list=[]
        for i in Bid.objects.all():
            bid={}
            bid['id']= i.bid
            bid['name']= i.name
            bid['group']= i.get_group_display()
            bid_list.append(bid)
        return Response(bid_list)

@api_view(['get'])
def Ctypelist(request):
    if request.method == 'GET':
        ctype_list=[]
        for i in Ctype.objects.all():
            ctype={}
            ctype['code']= i.ctype
            if i.ctype==4 or i.ctype==5 or i.ctype==6:
                ctype['ctype']="slow"
            else:
                ctype['ctype']="rapid"
            ctype['name']= i.get_ctype_display()
            ctype_list.append(ctype)
        return Response(ctype_list)

@api_view(['get'])
@permission_classes((IsKakao, ))
def ChargerPeakTime(request):
    if request.method == 'GET':
        datetime_now = datetime.now()
        two_weeks_ago=datetime_now + timedelta(days=-49)
        log_datas=CidstatLog.objects.filter(time__gte=two_weeks_ago, Cidstat__sid__sid=request.GET['sid'])
        peak_data={}
        week = ['mo', 'tu', 'we', 'th', 'fr', 'sa', 'su']
        hours = ['h1','h2','h3','h4','h5','h6','h7','h8','h9','h10','h11','h12','h13','h14','h15','h16','h17','h18','h19','h20','h21','h22','h23','h24']
        for day in week:
            peak_data[day]={}
            for hour in hours:
                peak_data[day][hour]=0

        for log_data in log_datas:
            try:
                print(peak_data[week[log_data.time.weekday()]]['h'+str(log_data.time.hour)])

                peak_data[week[log_data.time.weekday()]]['h'+str(log_data.time.hour+1)]=peak_data[week[log_data.time.weekday()]]['h'+str(log_data.time.hour+1)]+1
            except:
                peak_data[week[log_data.time.weekday()]]['h'+str(log_data.time.hour+1)]=1
        views=Viewtimes.objects.get(api_url="chargerpeaktime")
        views.views+=1
        views.save()

        return Response(peak_data)

@api_view(['get', 'post'])
def region(request):
    if request.method == 'GET':
        queryset=Charger.objects.exclude(bid='PW')
        filter_query={}
        for data in request.GET:
            if request.GET.get(data)!="":
                filter_query[data]=dict(request.GET)[data]
            filter_query.pop('view', None)
            filter_query.pop('format', None)
            filter_query.pop('stlat', None)
            filter_query.pop('enlat', None)
            filter_query.pop('stlng', None)
            filter_query.pop('enlng', None)
        
        statqs=queryset
        payqs=queryset
        bidqs=queryset
        ctypeqs=queryset
        if request.GET.get('stat') or request.GET.get('bid') or request.GET.get('ctype'):
            qs=queryset

            for i in filter_query:
                if i == 'ctype':
                    if "" in filter_query[i]:
                        filter_query[i].remove("")
                    if "," in filter_query[i][0]:
                        ctypeqs=qs.filter(ctype__in=filter_query[i][0].split(','))
                    else:
                        ctypeqs=qs.filter(ctype__in=filter_query[i])
                if i == 'stat':
                    if "" in filter_query[i]:
                        filter_query[i].remove("")
                    if "," in filter_query[i][0]:
                        statqs=qs.filter(stat__in=filter_query[i][0].split(','))
                    else:
                        statqs=qs.filter(stat__in=filter_query[i])
                    
                if i == 'pay':
                    if "" in filter_query[i]:
                        filter_query[i].remove("")
                    if "," in filter_query[i][0]:
                        payqs=qs.filter(pay__in=filter_query[i][0].split(','))
                    else:
                        payqs=qs.filter(pay__in=filter_query[i])
                if i == 'bid':
                    if "" in filter_query[i]:
                        filter_query[i].remove("")
                    if "," in filter_query[i][0]:
                        bidqs=qs.filter(bid__in=filter_query[i][0].split(','))
                    else:
                        bidqs=qs.filter(bid__in=filter_query[i])
                    
                    
        if request.GET.get('stlat') and  request.GET.get('enlat') and request.GET.get('stlng') and request.GET.get('enlng'):
            xyqueryset = Region.objects.filter(depth=request.GET.get('view'), lat__gte=request.GET.get('stlat'), lat__lte=request.GET.get('enlat'),lng__gte=request.GET.get('stlng'), lng__lte=request.GET.get('enlng'))

        else:
            xyqueryset = Region.objects.filter(depth=request.GET.get('view'))
        region_data=[]
        if request.GET['view'] is "1": 
            for region in xyqueryset:
                region_query=Charger.objects.filter(region1=region.region)
                regionqueryset=list(set(list(queryset&region_query&statqs&payqs&bidqs&ctypeqs)))
                
                region_data.append({"name":region.region, "count":len(regionqueryset),"lat":region.lat,"lng":region.lng})

            return Response(region_data)
        if request.GET['view'] is "2": 
            for region in xyqueryset:
                region2=region.region.split()
                region2_depth2=region.region.replace(region2[0]+' ','')
                region_query=Charger.objects.filter(region1=region2[0], region2=region2_depth2)

                if  "세종" in region.region:
                    region_query=Charger.objects.filter(region1=region2[0])
                regionqueryset=list(set(list(queryset&region_query&statqs&payqs&bidqs&ctypeqs)))
                if len(regionqueryset) is not 0:
                    region_data.append({"name":region2_depth2, "count":len(regionqueryset),"lat":region.lat,"lng":region.lng})
            return Response(region_data)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
@api_view(['get'])
def versioncheck(request):
    if request.method == 'GET':
        versions=VersionCheck.objects.all().order_by('-time')
        version={}
        if len(versions) != 0:
            version['version']= versions[0].version
            version['require']= versions[0].require
            version['note']= versions[0].note
        return Response(version)


def evonimg(request):
    if request.user.is_superuser:
        return render(request, 'all.html',)
    else:
        return HttpResponseRedirect("http://evon.kr")
def bidsvg(request, bid, stat):
    print(bid, stat)
    bid=bid.lower()
    content={
        'bid':bid,
        'stat':stat,
    }
    stat=int(stat)
    if stat==0 or stat==1 or stat==7 or stat==9:
        stat="obstacle"
    elif stat==2:
        stat="possibility"
    elif stat==3:
        stat="ing"
    elif stat==4:
        stat="stop"
    elif stat==5:
        stat="check"
    return render(request, 'bidmarker/marker-'+stat+'-logo-'+bid+'.svg',content, content_type="image/svg+xml")

@csrf_exempt
def imgdelete(request):
    if not request.user.is_superuser:
        return HttpResponseRedirect("http://evon.kr")
    sid=request.POST['sid']
    img_num=request.POST['img_num']
    charger=Charger.objects.get(sid=sid)
    if img_num=="1":
        charger.charger_img1="defalutChargerImg.jpg"
        charger.charger_img01="defalutChargerImg.jpg"
        charger.save()
    if img_num=="2":
        charger.charger_img2="defalutChargerImg.jpg"
        charger.charger_img02="defalutChargerImg.jpg"
        charger.save()
    if img_num=="3":
        charger.charger_img3="defalutChargerImg.jpg"
        charger.charger_img03="defalutChargerImg.jpg"
        charger.save()
    if img_num=="4":
        charger.charger_img4="defalutChargerImg.jpg"
        charger.charger_img04="defalutChargerImg.jpg"
        charger.save()
    return render(request, 'all.html',)
@csrf_exempt
def imgupload(request):
    if not request.user.is_superuser:
        return HttpResponseRedirect("http://evon.kr")
    sid= request.POST.get('sid')
    charger=Charger.objects.get(sid=sid)
    try:
        if request.FILES['charger_img1']:
            charger_img1 = request.FILES['charger_img1']

            resize_img=Image.open(charger_img1)
            #사이즈 지정
            ratio = resize_img.size[0]/720
            saro = resize_img.size[1]/ratio
            size = 720,saro
            #-----
            resize_img.thumbnail(size)
            buffer = BytesIO()
            resize_img.save(fp=buffer, format='JPEG')
            buff_val = buffer.getvalue()
            charger.charger_img1.save('a.jpg', ContentFile(buff_val))
    except:
        pass
    try:
        if request.FILES['charger_img2']:
            charger_img2 = request.FILES['charger_img2']

            resize_img=Image.open(charger_img2)
            #사이즈 지정
            ratio = resize_img.size[0]/720
            saro = resize_img.size[1]/ratio
            size = 720,saro
            #-----
            resize_img.thumbnail(size)
            buffer = BytesIO()
            resize_img.save(fp=buffer, format='JPEG')
            buff_val = buffer.getvalue()
            charger.charger_img2.save('a.jpg', ContentFile(buff_val))
    except:
        pass
    try:
        if request.FILES['charger_img3']:
            charger_img3 = request.FILES['charger_img3']

            resize_img=Image.open(charger_img3)
            #사이즈 지정
            ratio = resize_img.size[0]/720
            saro = resize_img.size[1]/ratio
            size = 720,saro
            #-----
            resize_img.thumbnail(size)
            buffer = BytesIO()
            resize_img.save(fp=buffer, format='JPEG')
            buff_val = buffer.getvalue()
            charger.charger_img3.save('a.jpg', ContentFile(buff_val))
    except:
        pass
    try:
        if request.FILES['charger_img4']:
            charger_img4 = request.FILES['charger_img4']

            resize_img=Image.open(charger_img4)
            #사이즈 지정
            ratio = resize_img.size[0]/720
            saro = resize_img.size[1]/ratio
            size = 720,saro
            #-----
            resize_img.thumbnail(size)
            buffer = BytesIO()
            resize_img.save(fp=buffer, format='JPEG')
            buff_val = buffer.getvalue()
            charger.charger_img4.save('a.jpg', ContentFile(buff_val))
    except:
        pass
    
    try:
        if request.FILES['charger_img1']:
            charger_img01 = request.FILES['charger_img1']
            charger.charger_img01=charger_img1
    except:
        pass
    try:
        if request.FILES['charger_img2']:
            charger_img02 = request.FILES['charger_img2']
            charger.charger_img02=charger_img2
    except:
        pass
    try:
        if request.FILES['charger_img3']:
            charger_img03 = request.FILES['charger_img3']
            charger.charger_img03=charger_img3
    except:
        pass
    try:
        if request.FILES['charger_img4']:
            charger_img04 = request.FILES['charger_img4']
            charger.charger_img04=charger_img4
    except:
        pass
    
    charger.save()
    return HttpResponseRedirect("/evonimg")
