from rest_framework import serializers

from .models import Charger, CidStat, CidstatLog, VersionCheck, ChargersNear, ChargerParking, Usetime

class ChargerSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Charger
        fields = ('sid', 'lng', 'lat', 'stat','bid')

class ChargerSearchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Charger
        fields = ('sid', 'name', 'addrDoro',  'lng', 'lat', 'bid')
        extra_kwargs = {
            'user': {'read_only': True},
        }
        

class CidStatSearchSerializer(serializers.ModelSerializer):
    class Meta:
        model = CidStat
        fields = ('sid', 'stat', 'cid', 'ctype')
        extra_kwargs = {
            'user': {'read_only': True},
        }

class CidStatForigenSerializer(serializers.ModelSerializer):
    cid = serializers.CharField(source='Cid')
    class Meta:
        model = CidStat
        fields = ( 'cid', 'stat' ,'ctype')
        extra_kwargs = {
            'user': {'read_only': True},
        }
class ChargernearForigenSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChargersNear
        fields = ( 'place_name', 'road_address_name' ,'category_group_code')
        extra_kwargs = {
            'user': {'read_only': True},
        }

class ChargerparkingForigenSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChargerParking
        fields = ( 'base', 'add', 'day' ,'detail')

class ChargerUsetimeForigenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usetime
        fields = ( 'weekday', 'sat', 'sun' ,'detail')
class ChargerlogSerializer(serializers.ModelSerializer):
    class Meta:
        model = CidstatLog
        fields = (  'stat_befor','stat_after' )
        extra_kwargs = {
            'user': {'read_only': True},
        }


class JSONSerializerField(serializers.Field):
    """ Serializer for JSONField -- required to make field writable"""
    def to_internal_value(self, data):
        return data
    def to_representation(self, value):
        return value
class ChargerSerializer(serializers.ModelSerializer):
    cidstat = CidStatForigenSerializer(many=True, read_only=True)
    chargernear = ChargernearForigenSerializer(many=True, read_only=True)
    count_avail = serializers.JSONField(source='Count_avail')
    class Meta:
        model = Charger
        fields = ('sid','name', 'ctype', 'addrDoro', 'lat', 'lng', 'pay', 'tel', 'tel2', 'LastUsedTime', 'bid', 'stat','cidstat','charger_img1', 'charger_img2', 'charger_img3', 'charger_img4', 'note', 'count_avail', 'detail_addr', 'chargernear')
        # 'newscard1_color', 'newscard1_content', 'newscard2_color', 'newscard2_content', 'newscard3_color', 'newscard3_content', 'newscard3_color', 'newscard4_color', 'newscard4_content', 'newscard5_color','newscard5_content',

class KakaoSerializer(serializers.ModelSerializer):
    cidstat = CidStatForigenSerializer(many=True, read_only=True)
    chargernear = ChargernearForigenSerializer(many=True, read_only=True)
    parking=ChargerparkingForigenSerializer(read_only=True)
    usetime=ChargerUsetimeForigenSerializer(read_only=True)
    bid=serializers.CharField(source='bid_str')
    daddr=serializers.CharField(source='addrDoro')
    daddrdtl=serializers.CharField(source='detail_addr')
    charger_imgs  = serializers.ListField(source='Charger_img_s')
    pay  = serializers.CharField(source='pay_str')
    
    def transform_show(self, obj, value):
        print(value+"1")
        return(value+"1")
    class Meta:
        model = Charger
        fields = ('sid','show','parking','name', 'ctype', 'daddr', 'daddrdtl','lat', 'lng', 'pay', 'tel',  'usetime', 'bid', 'cidstat','charger_imgs', 'note', 'chargernear')
        # 'newscard1_color', 'newscard1_content', 'newscard2_color', 'newscard2_content', 'newscard3_color', 'newscard3_content', 'newscard3_color', 'newscard4_color', 'newscard4_content', 'newscard5_color','newscard5_content',


class ReviewChargerSerializer(serializers.Serializer):
    sid = serializers.CharField(max_length=100)

class VersionCheckSerializer(serializers.ModelSerializer):
    class Meta:
        model = VersionCheck
        fields = (  'version' )
