from django.core.management.base import BaseCommand, CommandError
from django.contrib.gis.geos import Point
from chargers.models import Charger, CidStat, ChargerLog,CidstatLog
from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime
from django.core import serializers
from django.core.files.base import ContentFile
from urllib.request import urlopen
from io import BytesIO
import requests
import string
import json
from PIL import Image
from time import sleep

class Command(BaseCommand):


    def handle(self, *args, **options):
        # -*- coding: utf-8 -*- 
        chargerURL="http://www.chargev.co.kr/station-map/search/"
        custom_headers = {
	        'Accept-Encoding': 'gzip, deflate',
	        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
	        'X-Requested-With': 'XMLHttpRequest',
	        'Content-Length': '169'
        }
        data={"st":"address","chargev":"y","hchargev":"n","kps":"n","ministry":"n","ac3":"n","demo":"y","combo":"y","slow":"y","sido":"","gugun":"","name":""}
        charger_crawl = requests.post(chargerURL, data=data, headers=custom_headers)
        
        data_text=charger_crawl.text.replace('<script type="text/javascript">location.href = "https://www.chargev.co.kr/station-map/search/";</script>','')
        a=json.loads(data_text)
        count=0
        station={}
        for i in a["list"]:

            # -- 고유 id 추출 --
            sid = "2000"+i['station_idx']
            # -- 위도 추출 --
            lat = i['station_lat']
            # -- 경도 추출 --
            lng = i['station_lng']
            point=Point(float(lng),float(lat))
            # -- 주소 추출 --
            addr = i['station_address']
            detail_addr=i['station_detail_location']

            try:
                hash_charger = Charger.objects.get(sid=sid)
                

                region1=''
                region2=''
            except:
                try:
                    print(hash_charger+1)
                    url = "https://dapi.kakao.com/v2/local/geo/coord2regioncode.json?x="+lng+"&y="+lat
                    location_data=requests.get(url ,headers={'Authorization': 'KakaoAK b3fc8bbabf0c386c96be72eb5b270a4a'})
                    location_json = json.loads(location_data.text)
                    region1=location_json['documents'][0]['region_1depth_name']
                    print(region1)
                    region2=location_json['documents'][0]['region_2depth_name']
                except:
                    region1=''
                    region2=''
            # -- 충전소명 추출 --
            name = i['station_name']
            # -- 사용가능 충전 추출 --
            ctype=[]
            if i['charger_type_rapidity'] != "0":
                ctype.append(1)
                ctype.append(2)
                ctype.append(3)
            if i['charger_type_slow'] != "0":
                ctype.append(4)
            #충전소 생성
            
            point=Point(float(lng),float(lat))
            try:
                charger = Charger.objects.create(sid=sid, name = name, addrDoro=str(addr), region1=region1, region2=region2, lat=lat, lng=lng, useTime='', stat='0', point=point)
                charger.pay.set([1])
                charger.bid.set(['PI'])
                charger.ctype.set(ctype)
                
            except Exception as e:
                charger = Charger.objects.get(sid=sid)
                print(charger)
                if charger.edit == False:
                    charger.detail_addr= detail_addr
                    charger.save()
                #charger.tel1="1522-0123"
                #print(station['name']+" created")
            try:
                
                c_url="http://www.chargev.co.kr/station-map/station-view/"
                custom_headers = {
	                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
	                'X-Requested-With': 'XMLHttpRequest',
	                'Content-Length': '16'
                    
                }
                data={"station_idx": i['station_idx']}
                c_crawl = requests.post(c_url, data=data, headers=custom_headers)
                c_crawl_text=c_crawl.text.replace('<script type="text/javascript">location.href = "https://www.chargev.co.kr/station-map/station-view/";</script>','')
                a=json.loads(c_crawl_text)
                print(a)
                count=0
                for i in a['list']:
                    count+=1
                    if count!=1:
                        print(count)
                    if "완속" in i['charger_type']:
                            ctype=[4]
                    elif "급속" in i['charger_type']:
                            ctype=[1,2,3]

                    if "fixed" in i['charger_status']:
                        stat=4
                    elif "charging" in i['charger_status']:
                        stat=3
                    elif "standby" in i['charger_status']:
                        stat=2
                    else:
                        stat=0
                    try:
                        try:
                            station[sid]+=1
                            cid_charger = CidStat.objects.create(sidcid=sid+str(station[sid]), cid=str(station[sid]), stat=stat,sid=charger)
                            cid_charger.ctype.set(ctype)
                        except:

                            station[sid]=1

                            cid_charger = CidStat.objects.create(sidcid=sid+str(station[sid]), cid=str(station[sid]), stat=stat,sid=charger)
                            cid_charger.ctype.set(ctype)
                    except:
                        cid_charger = CidStat.objects.get(sidcid=sid+str(station[sid]))
                        cid_charger.stat = stat
                        print(stat)
                        cid_charger.save()
            except:

                pass