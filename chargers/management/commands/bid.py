from django.core.management.base import BaseCommand, CommandError
from django.contrib.gis.geos import Point
from chargers.models import Charger, CidStat, ChargerLog
class Command(BaseCommand):
    def handle(self, *args, **options):
    	BID =(
    	('BG', '비긴스'),
		('JE', '제주전기자동차서비스'),
		('KP', '한국전력'),
		('HE', '한국전기차충전서비스'),
		('PI', '포스코ICT'),
		('SE', '서울시'),
		('PW', '파워큐브'),
		('CT', '씨티카'),
		('ME', '환경부'),
		('JD', '제주특별자치도청'),
		('TM', '티맵'),
		('HM', '현대자동차네비게이션'),
		('GI', '기아자동차'),
		('HD', '현대자동차'),
		('DG', '대구환경공단'),
		('SS', '수원시'),
		('ST', '에스트래픽'),
		('GN', '지엔텔'),
		('KT', '케이티'),
		('ER', '에버온'),
		('WR', '울릉군청'),
		('HO', '수소충전소'),
		('IC', '인천국제공항'),
		('SG', '시그넷'),
		('DY', '대영채비'),
		('CL', '클린일렉스'),
		('MT', '환경부시범운영'),
        ('TE', '테슬라'),
        ('BM', 'BMW'),
        ('IK', '이케아'),
        ('EC', '이카플러그'),
        ('BE', '보리타에너지'),
        ('BZ', '벤츠'),
        ('EC', '기타'),

    	)
            for i in BID:
            try:
                Bid.objects.create(bid=i[0])
                print(i[1])
            except:
                pass