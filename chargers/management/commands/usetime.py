from django.core.management.base import BaseCommand, CommandError
from django.contrib.gis.geos import Point
from chargers.models import Charger, CidStat, ChargerLog, CidstatLog, Usetime
from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime
from django.core import serializers
import requests
import string
import json
import os
from time import sleep
import openpyxl

class Command(BaseCommand):


    def handle(self, *args, **options):
        # -*- coding: utf-8 -*- 
        wb = openpyxl.load_workbook('ev.xlsx')
        ws = wb.active
        logs = CidstatLog.objects.all()
        row=2
        for log in logs:
            usetime=[]
            name = ws.cell(row=row, column=1).value
            tel = ws.cell(row=row, column=2).value
            wee = ws.cell(row=row, column=3).value
            sat = ws.cell(row=row, column=4).value
            sun = ws.cell(row=row, column=5).value
            note = ws.cell(row=row, column=6).value
            try:
                charger = Charger.objects.get(name=name)
            except Exception as e:
                row+=1
                print(e)
                continue
                
            #-----------------------평일------------------------
            if not wee == None:
                weetime=wee.split("~")
                if len(weetime) == 2:
                    time1 = Usetime.objects.filter(day="wee", start_time=weetime[0], end_time=weetime[1])
                    if len(time1):
                        usetime.append(time1[0].pk)
                    else:
                        time1 = Usetime.objects.create(day="wee", start_time=weetime[0], end_time=weetime[1])
                        usetime.append(time1.pk)
            #--------------------------------------------------

            #-----------------------토요일-----------------------
            if not sat == None:
                sattime=sat.split("~")
            
                if len(sattime) == 2:
                    time2 = Usetime.objects.filter(day="sat", start_time=sattime[0], end_time=sattime[1])
                    if len(time2):
                        usetime.append(time2[0].pk)
                    else:
                        time2 = Usetime.objects.create(day="sat", start_time=sattime[0], end_time=sattime[1])
                        usetime.append(time2.pk)
            
            #--------------------------------------------------

            #-----------------------일요일-----------------------
            if not sun == None:
                suntime=sun.split("~")
                if len(suntime) == 2:
                    time3 = Usetime.objects.filter(day="sun", start_time=suntime[0], end_time=suntime[1])
                    if len(time3):
                        usetime.append(time3[0].pk)
                    else:
                        time3 = Usetime.objects.create(day="sun", start_time=suntime[0], end_time=suntime[1])
                        usetime.append(time3.pk)
            #--------------------------------------------------
            print(charger)
            print(usetime)
            charger.usetime.set(usetime)
            row+=1
            if(ws.cell(row=row, column=1).value==None):
                break
        
        wb.close()