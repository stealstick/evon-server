from django.core.management.base import BaseCommand, CommandError
from chargers.models import Charger, CidStat, ChargerLog, Usetime,Bid
from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime
from django.core import serializers
import requests
import string
import json
from time import sleep

class Command(BaseCommand):


    def handle(self, *args, **options):
        # -*- coding: utf-8 -*- 
        charger_count=0
        happeURL="http://happecharger.com/chargingInfra/getCsInfo?_REQ_DATA_TYPE_=json&_USE_WRAPPED_OBJECT_=true"
        data={'_REQ_JSON_OBJECT_': '{"area_cd":null,"warea_cd":"","cs_tp":"","cs_nm":"","comp1":"HE","comp2":null,"comp3":null,"charge1":"1","charge2":"2","charge3":"3","charge4":"4"}'}
        happe_crawl = requests.post('http://happecharger.com/chargingInfra/getCsInfo?_REQ_DATA_TYPE_=json&_USE_WRAPPED_OBJECT_=true',data = data)
        happe_data = happe_crawl.text
        happe_json = json.loads(happe_data)
        progress=0
        progress_percent=0
        a=[]
        for happe_data in happe_json['data']['list']:
            progress = progress+1
            progress_percent=str(int(100*(progress/len(happe_json['data']['list']))))
            progress_count="("+str(progress)+"/"+str(len(happe_json['data']['list']))+")"
            print("happe " + progress_percent+"%"+progress_count)
            cid=0
            try:
                try:
                    sid=str(70000000)+str(happe_data['cs_id'])
                    hash_charger = Charger.objects.get(sid=sid)
                    if hash_charger.edit==False:
                        addr=str(happe_data['cs_addr'])
                        hash_charger.addrDoro = addr
                        hash_charger.lat = happe_data['cs_lati']
                        hash_charger.lng = happe_data['cs_longi']
                        hash_charger.bid.set(['HE'])
                        hash_charger.save()
                    charger_url="http://happecharger.com/chargingInfra/getCpInfo?_REQ_DATA_TYPE_=json&_USE_WRAPPED_OBJECT_=true"
                    
                    charger_cs_id = '{"cs_id":"'+happe_data['cs_id']+'","charge_tp":""}'
                    charger_data={'_REQ_JSON_OBJECT_':charger_cs_id}
                    charger_happe_crawl = requests.post(charger_url,charger_data)
                    charger_happe_data = charger_happe_crawl.text
                    
                    charger_happe_json = json.loads(charger_happe_data)
                except:

                    url = "https://dapi.kakao.com/v2/local/geo/coord2regioncode.json?x="+happe_data['cs_longi']+"&y="+happe_data['cs_lati']
                    location_data=requests.get(url ,headers={'Authorization': 'KakaoAK b3fc8bbabf0c386c96be72eb5b270a4a'})
                    location_json = json.loads(location_data.text)
                    print(happe_data)
                    addr=str(happe_data['cs_addr'])
                    print(location_json)
                    region1=location_json['documents'][0]['region_1depth_name']
                    region2=location_json['documents'][0]['region_2depth_name']
                    print('sdf')
                    
                    addr=str(happe_data['cs_addr'])
                    sid=str(70000000)+str(happe_data['cs_id'])
                    print(sid)
                    usetimew=Usetime.objects.create(weekday="", sat="", sun="", detail="")
                    print(usetimew)

                    hash_charger = Charger.objects.create(sid=sid, usetime=usetimew, name = happe_data['cs_nm'], addrDoro=addr, region1=region1, region2=region2, lat=happe_data['cs_lati'], lng=happe_data['cs_longi'],  LastUsedTime="",  stat=0)
                    print("afafafa")
                    hash_charger.bid.set([Bid.objects.get(bid="HE")])
                    
                charger_ctype_list=[]
                for happe_charger in charger_happe_json['data']['list']:
                    charger_count+=1
                    cid+=1
                    a.append(happe_charger['cp_status'])
                    ctype_list=[]
                    if "DC차데모" in happe_charger['charge_mthd_cd']:
                        ctype_list.append(1)
                        charger_ctype_list.append(1)
                    if "DC콤보" in happe_charger['charge_mthd_cd']:
                        ctype_list.append(2)
                        charger_ctype_list.append(2)
                    if "AC3상" in happe_charger['charge_mthd_cd']:
                        ctype_list.append(3)
                        charger_ctype_list.append(3)
                    if "BC타입" in happe_charger['charge_mthd_cd']:
                        ctype_list.append(4)
                        charger_ctype_list.append(4)
                    if "C타입" in happe_charger['charge_mthd_cd']:
                        ctype_list.append(4)
                        charger_ctype_list.append(4)
                    if "B타입" in happe_charger['charge_mthd_cd']:
                        ctype_list.append(4)
                        charger_ctype_list.append(4)
                        n_c1='Y'
                        n_t1='완속 B타입을 사용하려면 개인용 차량연결 커넥터가 필요합니다.'
                    charger_stat=3
                    if "완료" in happe_charger['cp_status'] or "사용가능" in happe_charger['cp_status']:
                        cid_stat=2
                        charger_stat=2
                    elif "연결안됨" in happe_charger['cp_status'] or "고장" in happe_charger['cp_status']:
                        cid_stat=0
                        charger_stat=0
                    else:
                        cid_stat=3
                        if charger_stat == 2:
                            charger_stat=2


                    try:
                        
                        happe_charger_cid = CidStat.objects.create(sidcid=sid+"0"+str(cid), cid=str(cid),  stat=cid_stat,sid=hash_charger)#happe_charger['cs_status']
                        
                    except:
                        happe_charger_cid = CidStat.objects.get(sidcid=sid+"0"+str(cid))
                        if ctype_list == []:
                            print(happe_charger['charge_mthd_cd'])
                        happe_charger_cid.ctype.set(ctype_list)
                        
                        happe_charger_cid.stat=cid_stat
                        happe_charger_cid.save()
                charger_ctype_list=list(set(charger_ctype_list))
                
                n_c1=''
                n_t1=''

                hash_charger.stat=charger_stat
                if hash_charger.edit==False:
                    hash_charger.ctype.set(charger_ctype_list)
                    hash_charger.pay.set([1])
                    hash_charger.bid.set(['HE'])
                    hash_charger.name=happe_data['cs_nm']
                hash_charger.save()
            except  Exception as e:
                print(e, "err")
                pass
        
        print("all charger :"+str(charger_count))
        print(set(a))