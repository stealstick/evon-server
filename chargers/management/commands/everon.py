from django.core.management.base import BaseCommand, CommandError
from django.contrib.gis.geos import Point
from chargers.models import Charger, CidStat, ChargerLog, CidstatLog
from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime
from django.core import serializers
from django.core.files.base import ContentFile
from urllib.request import urlopen
from io import BytesIO
import requests
import string
import json
from PIL import Image
from time import sleep

class Command(BaseCommand):


    def handle(self, *args, **options):
        # -*- coding: utf-8 -*- 
        chargerURL="https://www.everon.co.kr/service/charger/getChargerInfoList"
        data={"sido":"","sigungu":"-","addr":"-","sponam":"","chatypes[]":["CHRA02","CHRA02","CHRA01","CHRA07","CHRA04"]}
        charger_crawl = requests.post(chargerURL, data=data)
        a=json.loads(charger_crawl.text)
        count=0
        station={}
        for i in a:
            # -- 고유 id 추출 --
            sid = i['id']
            # -- 위도 추출 --
            lat = i['lat']
            # -- 경도 추출 --
            lng = i['lon']
            point=Point(float(lng),float(lat))
            # -- 주소 추출 --
            addr = i['add1']
            addr2 = i['add2']
            detail_addr = i['pladesc']
            try:
                hash_charger = Charger.objects.get(sid=sid)
                if hash_charger.edit == False:
                    hash_charger.detail_addr = detail_addr
                    hash_charger.addr2=str(addr2)
                    hash_charger.save()
            except:
                try:
                    url = "https://dapi.kakao.com/v2/local/geo/coord2regioncode.json?x="+lng+"&y="+lat
                    location_data=requests.get(url ,headers={'Authorization': 'KakaoAK b3fc8bbabf0c386c96be72eb5b270a4a'})
                    location_json = json.loads(location_data.text)
                    region1=location_json['documents'][0]['region_1depth_name']
                    region2=location_json['documents'][0]['region_2depth_name']
                except:
                    region1=''
                    region2=''
            # -- 충전소명 추출 --
            name = i['sponam']
            print(name)
            point=Point(float(lng),float(lat))
            try:
                charger = Charger.objects.create(sid=sid, name = name, addrDoro=str(addr), addr2=str(addr2), region1=region1, region2=region2, lat=lat, lng=lng, useTime='', stat='0', point=point)
                charger.pay.set([1])
                charger.bid.set(['ER'])
                charger.ctype.set(ctype)
            except Exception as e:
                charger = Charger.objects.get(sid=sid)
            try:
                try:
                    count=1
                    CidStat.objects.filter(sid=charger)
                    chargerURL="https://www.everon.co.kr/service/chager/getDetailChargerInfoList"
                    
                    data={"spoid":i['spoid'], "chatypes[]":["CHRA02","CHRA02","CHRA01","CHRA07","CHRA04"]}
                    charger_crawl = requests.post(chargerURL, data=data)
                    charger_json = json.loads(charger_crawl.text)
                    for j in charger_json['info']['chargerList']:
                        print(sid+str(count))
                        cid_charger = CidStat.objects.get(sidcid=sid+str(count))
                        print(sid+str(count))
                        if "사용가능" in j['available']:
                            stat=2
                        elif "사용중" in j['available']:
                            stat=3
                        else:
                            print(j['available'])
                            stat=0
                        if cid_charger.stat != stat:
                            hash_charger = CidstatLog.objects.create(Cidstat=cid_charger, stat_before=cid_charger.stat, stat_after=stat)
                            
                        cid_charger.stat=stat
                        cid_charger.save()
                        count+=1
                except:
                    chargerURL="https://www.everon.co.kr/service/chager/getDetailChargerInfoList"
                    data={"spoid":i['spoid'], "chatypes[]":["CHRA02","CHRA02","CHRA01","CHRA07","CHRA04"]}

                    count=1
                    charger_crawl = requests.post(chargerURL, data=data)
                    charger_json = json.loads(charger_crawl.text)
                    charger_ctype_list=[]
                    print(i)
                    for j in charger_json['info']['chargerList']:
                        if j['available']=="사용가능":
                            stat=2
                        if j['available']=="사용중":
                            stat=3
                        else:
                            stat=0
                        cid_charger = CidStat.objects.create(sidcid=sid+str(count), cid=str(count), stat=stat,sid=charger)
                        if "완속" in j['chatype']:
                            cid_charger.ctype.set([4])
                            charger_ctype_list.append(4)
                        elif "DC" in j['chatype']:
                            cid_charger.ctype.set([1,2,3])
                            charger_ctype_list.append(1)
                            charger_ctype_list.append(2)
                            charger_ctype_list.append(3)
                        else:
                            cid_charger.ctype.set([1,2,3])
                            
                            charger_ctype_list.append(1)
                            charger_ctype_list.append(2)
                            charger_ctype_list.append(3)
                        charger.ctype.set(charger_ctype_list)
                        count+=1
            except:
                pass