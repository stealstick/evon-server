from django.core.management.base import BaseCommand, CommandError
from chargers.models import Charger, CidStat
from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime
from django.core import serializers
from bs4 import BeautifulSoup
import requests
from io import BytesIO
import urllib.request
from PIL import Image

from urllib.request import urlopen
from django.core.files.base import ContentFile
from time import sleep

class Command(BaseCommand):
    def handle(self, *args, **options):
        # -*- coding: utf-8 -*- 
        queryset=Charger.objects.all()
        for charger in queryset:
                chargerURL="http://ev.or.kr/portal/monitor/stationinfo?sid="+charger.sid
                try:
                    charger_crawl=requests.get(chargerURL)
                    charger_html=charger_crawl.text
                    soup = BeautifulSoup(charger_html, 'html.parser')
                    pic=soup.find_all(alt="충전소 사진")
                    img_locate=str(pic[0]['src'])
                    img_url="http://ev.or.kr" + img_locate
                    img01=urlopen(img_url)
                    print(img_url)
                    img_file = BytesIO(img01.read())
                    resize_img=Image.open(img_file)
                    #사이즈 지정
                    ratio = resize_img.size[0]/720
                    saro = resize_img.size[1]/ratio
                    size = 720,saro
                    #-----
                    resize_img.thumbnail(size)
                    buffer = BytesIO()
                    resize_img.save(fp=buffer, format='JPEG')
                    buff_val = buffer.getvalue()
                    charger.charger_img1.save('a.jpg', ContentFile(buff_val))
                    charger.save()
                    #-----
                    print("suc")
                    img_locate=str(pic[1]['src'])
                    img_url="http://ev.or.kr"+img_locate

                    img02=urlopen(img_url)
                    
                    img_file = BytesIO(img02.read())
                    resize_img=Image.open(img_file)
                    #사이즈 지정
                    ratio = resize_img.size[0]/720
                    saro = resize_img.size[1]/ratio
                    size = 720,saro
                    #-----
                    resize_img.thumbnail(size)
                    buffer = BytesIO()
                    resize_img.save(fp=buffer, format='JPEG')
                    buff_val = buffer.getvalue()
                    charger.charger_img2.save('a.jpg', ContentFile(buff_val))
                    charger.save()
                    print("suc")


                except:
                    pass
