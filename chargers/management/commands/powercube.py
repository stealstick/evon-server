from django.core.management.base import BaseCommand, CommandError
from django.contrib.gis.geos import Point
from chargers.models import Charger, CidStat, ChargerLog
from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime
from django.core import serializers
import requests
import string
import json
from time import sleep

class Command(BaseCommand):


    def handle(self, *args, **options):
        # -*- coding: utf-8 -*- 
        powercubeURL="https://www.ev-line.co.kr/charge/mapdataadd.asp"
        powercube_crawl = requests.get(powercubeURL)   
        powercube_data = powercube_crawl.text
        powercube_json = json.loads(powercube_data)
        progress=0
        progress_percent=0
        for powercube_data in powercube_json:
            progress = progress+1
            progress_percent=str(int(100*(progress/len(powercube_json))))
            progress_count="("+str(progress)+"/"+str(len(powercube_json))+")"
            print("powercube " + progress_percent+"%"+progress_count)
            try:
                try:
                    hash_ch=Charger.objects.get(sid=powercube_data['id'])
                    ctype_list=[4]
                    bid=['PW']
                    pay=[1]
                    if hash_ch.edit == False:
                        hash_ch.bid.set(bid)
                        hash_ch.pay.set(pay)
                        hash_ch.ctype.set(ctype_list)
                except:
                    
                    url = "https://dapi.kakao.com/v2/local/geo/coord2regioncode.json?x="+powercube_data['lat']+"&y="+powercube_data['lng']
                    location_data=requests.get(url ,headers={'Authorization': 'KakaoAK b3fc8bbabf0c386c96be72eb5b270a4a'})
                    location_json = json.loads(location_data.text)
                    if not location_json['errorType']:
                        print(1)
                        sleep(0.5)
                        addr=region2=location_json['documents'][0]['address_name']
                        region1=location_json['documents'][0]['region_1depth_name']
                        region2=location_json['documents'][0]['region_2depth_name']
                        print(2)
                        point=Point(float(powercube_data['lat']),float(powercube_data['lng']))
                        ctype_list=[4]
                        hash_charger = Charger.objects.create(sid=powercube_data['id'], name = powercube_data['cont'], addrDoro=addr, region1=region1, region2=region2, lat=powercube_data['lng'], lng=powercube_data['lat'], useTime="24시간 이용가능", LastUsedTime="09:00", stat=2, point=point)
                        print(2)
                        bid=['PW']
                        pay=[1]
                        hash_ch.bid.set(bid)
                        hash_ch.pay.set(pay)
                        hash_ch.ctype.set(ctype_list)
                        hash_charger.save
                        print(3)
            except Exception as e:
                pass