from django.core.management.base import BaseCommand, CommandError
from chargers.models import Charger,CidStat
from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime
from django.core import serializers
from bs4 import BeautifulSoup
import requests
import urllib.request
from time import sleep
from urllib.request import urlopen
from django.core.files.base import ContentFile

class Command(BaseCommand):
    def handle(self, *args, **options):
       # -*- coding: utf-8 -*- 
        queryset=Charger.objects.all()
        progress=0
        for charger in queryset:
            chargerURL="http://ev.or.kr/portal/monitor/stationinfo?sid="+charger.sid
            progress = progress+1
            progress_percent=str(int(100*(progress/len(queryset))))
            progress_count="("+str(progress)+"/"+str(len(queryset))+")"
            print(progress_percent+"%"+progress_count)
            if charger.bid=="파워큐브":
                continue
            
            try:
                charger_crawl=requests.get(chargerURL)

                print("bid")
                charger_html=charger_crawl.text
                soup = BeautifulSoup(charger_html, "html.parser")
                memo=soup.select(" #wrap > div > section > div:nth-of-type(5) > table > tr:nth-of-type(5) > td")
                call=soup.select(" #wrap > div > section > div:nth-of-type(5) > table > tr:nth-of-type(3) > td")
                if not charger.edit:
                    pay=soup.select(" #wrap > div > section > div:nth-of-type(5) > table > tr:nth-of-type(4) > td")
                    #print(charger.sid, memo[0].text.strip())
                    memo=str(memo[0].text.strip())
                    call=str(call[0].text.strip())
                    pay=str(pay[0].text.strip())
                    charger.note=memo
                    charger.tel='02-1833-8017'
                    pay=[1]
                    charger.pay.set(pay)
                
                print("bid")
                bid=list(charger.bid.all())[0].bid
                print(bid)
                if bid:
                    if bid == "ME":
                        charger.tel = "1661-9408"
                    if bid == "SE":
                        charger.tel = "1661-9408"
                    if bid == "JE":
                        charger.tel = "064-723-0853"
                    if bid == "KP":
                        charger.tel = "1899-2100"
                    if bid == "HE":
                        charger.tel = "1522-1782"
                    if bid == "PI":
                        charger.tel = "1600-4047"
                    if bid == "JD":
                        charger.tel = "1899-8852"
                    if bid == "DG":
                        charger.tel = "053-605-8060"
                    if bid == "SS":
                        charger.tel = "1661-9408"
                    if bid == "ST":
                        charger.tel = "031-601-3500"
                    if bid == "GN":
                        charger.tel = "1544-4279"
                    if bid == "KT":
                        charger.tel = "1522-4700"
                    if bid == "ER":
                        charger.tel = "1661-7766"
                    if bid == "WR":
                        charger.tel = "031-723-4652"
                    if bid == "IC":
                        charger.tel = "032-741-2805"
                    if bid == "DY":
                        charger.tel = "1522-2573"
                    if bid == "CL":
                        charger.tel = "1811-1350"
                    if bid == "MT":
                        charger.tel = "1661-9408"
                    if bid == "EC":
                        charger.tel = "031-778-6181"
                    print(charger.tel)
                    
                    
                
                charger.save()
                
            except:
                print("?")
                sleep(2)
                pass