from django.core.management.base import BaseCommand, CommandError
from django.contrib.gis.geos import Point
from chargers.models import Charger, CidStat, ChargerLog,CidstatLog
from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime
from django.core import serializers
from django.core.files.base import ContentFile
from urllib.request import urlopen
from io import BytesIO
import requests
import string
import json
from PIL import Image
from time import sleep

class Command(BaseCommand):


    def handle(self, *args, **options):
        # -*- coding: utf-8 -*- 
        chargerURL="https://chargingmate.com/KT_EVCP_FO/service/main/searchStationOpenApiInfo.json"
        data={"chargeTypeStatus":"02", "status":"P,I,N,Z","addrSearchKey":"","operator":"SK","bachgTypeCd":"01,02,03,04,05,06,07","bachgSttusCd":"0,1,2,3,4,5,6"}
        charger_crawl = requests.post(chargerURL, data=data)
        a=json.loads(charger_crawl.text)
        count=0
        station={}
        for i in a["ktData"]:
            # -- 고유 id 추출 --
            sid = i['chgplId']
            # -- 위도 추출 --
            lat = i['chgplLatitCrd']
            # -- 경도 추출 --
            lng = i['chgplLngitCrd']
            point=Point(float(lng),float(lat))
            # -- 주소 추출 --
            addr = i['chgplAddrType2']

            try:
                hash_charger = Charger.objects.get(sid=sid)
            except:
                try:
                    url = "https://dapi.kakao.com/v2/local/geo/coord2regioncode.json?x="+lng+"&y="+lat
                    location_data=requests.get(url ,headers={'Authorization': 'KakaoAK b3fc8bbabf0c386c96be72eb5b270a4a'})
                    location_json = json.loads(location_data.text)
                    region1=location_json['documents'][0]['region_1depth_name']
                    region2=location_json['documents'][0]['region_2depth_name']
                except:
                    region1=''
                    region2=''
            # -- 충전소명 추출 --
            name = i['chgplNm']
            # -- 사용가능 충전 추출 --
            ctype = [4]
            if i['status']=='P':
                stat=2
            elif i['status']=='Z':
                stat=4
                try:
                    print(hash_charger)
                    #hash_charger.delete()
                    print('sdf')

                    continue
                except:
                    continue
            elif i['status']=='I':
                stat=3
            else:
                print(i['status'])
                stat=0
            #충전소 생성
            
            point=Point(float(lng),float(lat))
            try:
                if name in "벤츠":
                    continue
                charger = Charger.objects.create(sid=sid, name = name, addrDoro=str(addr), region1=region1, region2=region2, lat=lat, lng=lng, useTime='', stat='0', point=point)
                charger.pay.set([1])
                charger.bid.set(['KT'])
                charger.ctype.set(ctype)
            except Exception as e:
                charger = Charger.objects.get(sid=sid)
                charger.tel1="1522-0123"
                #print(station['name']+" created")
                #print(str(e))
                count+=1
                pass
            try:
                try:
                    station[sid]+=1
                    print(sid, station[sid])

                    cid_charger = CidStat.objects.create(sidcid=sid+str(station[sid]), cid=str(station[sid]), stat=stat,sid=charger)
                    cid_charger.ctype.set([4])
                except:
                
                    station[sid]=1
                    print(sid, station[sid])
                    cid_charger = CidStat.objects.create(sidcid=sid+str(station[sid]), cid=str(station[sid]), stat=stat,sid=charger)
                    cid_charger.ctype.set([4])
            except:
                cid_charger = CidStat.objects.get(sidcid=sid+str(station[sid]))
                cid_charger.stat = stat
                cid_charger.save()

            try:
                if("defalutChargerImg.jpg" == charger.charger_img1):
                    img01=urlopen('https://chargingmate.com'+i['filePath'])
                    img_file = BytesIO(img01.read())
                    resize_img=Image.open(img_file)
                    ratio = resize_img.size[0]/720
                    saro = resize_img.size[1]*ratio
                    size = 720,saro
                    resize_img.thumbnail(size)
                    buffer = BytesIO()
                    resize_img.save(fp=buffer, format='JPEG')
                    buff_val = buffer.getvalue()
                    charger.charger_img1.save('a.jpg', ContentFile(buff_val))
                    charger.save()
                    #-----
                    img01=urlopen('https://chargingmate.com'+i['filePath1'])
                    img_file = BytesIO(img01.read())
                    resize_img=Image.open(img_file)
                    ratio = resize_img.size[0]/720
                    saro = resize_img.size[1]*ratio
                    size = 720,saro
                    resize_img.thumbnail(size)
                    buffer = BytesIO()
                    resize_img.save(fp=buffer, format='JPEG')
                    buff_val = buffer.getvalue()
                    charger.charger_img2.save('a.jpg', ContentFile(buff_val))
                    charger.save()
                    #-----
                    img01=urlopen('https://chargingmate.com'+i['filePath3'])
                    img_file = BytesIO(img01.read())
                    resize_img=Image.open(img_file)
                    ratio = resize_img.size[0]/720
                    saro = resize_img.size[1]*ratio
                    size = 720,saro
                    resize_img.thumbnail(size)
                    buffer = BytesIO()
                    resize_img.save(fp=buffer, format='JPEG')
                    buff_val = buffer.getvalue()
                    charger.charger_img3.save('a.jpg', ContentFile(buff_val))
                    charger.save()
            except:
                pass
            
        chargerURL="https://chargingmate.com/KT_EVCP_FO/service/main/searchStationOpenApiInfo.json"
        data={"chargeTypeStatus":"01|05|04", "status":"P,I,N,Z","addrSearchKey":"","operator":"SK","bachgTypeCd":"01,02,03,04,05,06,07","bachgSttusCd":"0,1,2,3,4,5,6"}
        charger_crawl = requests.post(chargerURL, data=data)
        a=json.loads(charger_crawl.text)
        count=0
        for i in a["ktData"]:
            # -- 고유 id 추출 --
            sid = i['chgplId']
            # -- 위도 추출 --
            lat = i['chgplLatitCrd']
            # -- 경도 추출 --
            lng = i['chgplLngitCrd']
            point=Point(float(lng),float(lat))
            # -- 주소 추출 --
            addr = i['chgplAddrType2']
            
            try:
                hash_charger = Charger.objects.get(sid=sid)
            except:
                try:
                    url = "https://dapi.kakao.com/v2/local/geo/coord2regioncode.json?x="+lng+"&y="+lat
                    location_data=requests.get(url ,headers={'Authorization': 'KakaoAK b3fc8bbabf0c386c96be72eb5b270a4a'})
                    location_json = json.loads(location_data.text)
                    region1=location_json['documents'][0]['region_1depth_name']
                    region2=location_json['documents'][0]['region_2depth_name']
                except:
                    region1=''
                    region2=''
            # -- 충전소명 추출 --
            name = i['chgplNm']
            # -- 사용가능 충전 추출 --
            ctype = [1,2,3]
            stat=0
            if i['status']=='P':
                stat=2
            elif i['status']=='Z':
                stat=4
                try:
                    print(hash_charger)
                    #hash_charger.delete()
                    print('sdf')
                    continue
                except:
                    continue
            elif i['status']=='I':
                stat=3
            else:
                print(i['status'])
                stat=0






            #충전소 생성
            
            point=Point(float(lng),float(lat))
            try:
                charger = Charger.objects.create(sid=sid, name = name, addrDoro=str(addr), region1=region1, region2=region2, lat=lat, lng=lng, useTime='', stat='0', point=point)
                charger.pay.set([1])
                charger.bid.set(['KT'])
                charger.ctype.set(ctype)
            except Exception as e:
                charger = Charger.objects.get(sid=sid)
                charger.tel="1522-0123"
                #print(station['name']+" created")
                #print(str(e))
                count+=1
                pass
            try:
                try:
                    station[sid]+=1
                    print(sid, station[sid])

                    cid_charger = CidStat.objects.create(sidcid=sid+str(station[sid]), cid=str(station[sid]), stat=stat,sid=charger)
                    cid_charger.ctype.set([1,2,3])
                except:
                
                    station[sid]=1
                    print(sid, station[sid])
                    cid_charger = CidStat.objects.create(sidcid=sid+str(station[sid]), cid=str(station[sid]), stat=stat,sid=charger)
                    cid_charger.ctype.set([1,2,3])
            except:
                cid_charger = CidStat.objects.get(sidcid=sid+str(station[sid]))
                if cid_charger.stat != stat:
                    hash_charger = CidstatLog.objects.create(Cidstat=cid_charger, stat_before=cid_charger.stat, stat_after=stat)
                cid_charger.stat = stat
                cid_charger.save()

            try:
                if("defalutChargerImg.jpg" == charger.charger_img1):
                    img01=urlopen('https://chargingmate.com'+i['filePath'])
                    img_file = BytesIO(img01.read())
                    resize_img=Image.open(img_file)
                    ratio = resize_img.size[0]/720
                    saro = resize_img.size[1]*ratio
                    size = 720,saro
                    resize_img.thumbnail(size)
                    buffer = BytesIO()
                    resize_img.save(fp=buffer, format='JPEG')
                    buff_val = buffer.getvalue()
                    charger.charger_img1.save('a.jpg', ContentFile(buff_val))
                    charger.save()
                    #-----
                    img01=urlopen('https://chargingmate.com'+i['filePath1'])
                    img_file = BytesIO(img01.read())
                    resize_img=Image.open(img_file)
                    ratio = resize_img.size[0]/720
                    saro = resize_img.size[1]*ratio
                    size = 720,saro
                    resize_img.thumbnail(size)
                    buffer = BytesIO()
                    resize_img.save(fp=buffer, format='JPEG')
                    buff_val = buffer.getvalue()
                    charger.charger_img2.save('a.jpg', ContentFile(buff_val))
                    charger.save()
                    #-----
                    img01=urlopen('https://chargingmate.com'+i['filePath3'])
                    img_file = BytesIO(img01.read())
                    resize_img=Image.open(img_file)
                    ratio = resize_img.size[0]/720
                    saro = resize_img.size[1]*ratio
                    size = 720,saro
                    resize_img.thumbnail(size)
                    buffer = BytesIO()
                    resize_img.save(fp=buffer, format='JPEG')
                    buff_val = buffer.getvalue()
                    charger.charger_img3.save('a.jpg', ContentFile(buff_val))
                    charger.save()
            except:
                pass
            