from django.core.management.base import BaseCommand, CommandError
from chargers.models import CidStat, Charger, CidstatLog
from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime
from django.core import serializers
import requests
import string
import json
from time import sleep




class Command(BaseCommand):

    def handle(self, *args, **options):
        # -*- coding: utf-8 -*- 
        chargerURL="http://ev.or.kr/portal/monitor/chargerList"
        charger_crawl = requests.get(chargerURL)   
        chargers_data = charger_crawl.text
        chargers_json = json.loads(chargers_data)
        progress = 0
        for charger_data in chargers_json['chargerList']:
            progress = progress+1
            progress_percent=str(int(100*(progress/len(chargers_json['chargerList']))))
            progress_count="("+str(progress)+"/"+str(len(chargers_json['chargerList']))+")"
            print(progress_percent+"%"+progress_count)
            print(0)
            #station=Charger.objects.get(sid=charger_data['sid'])
            #print(station)

            try:
                station=Charger.objects.get(sid=charger_data['sid'])
                if charger_data['cst'] == 7:
                    #print(station)
                    bid=['MT']
                    station.bid.set(bid)
                    charger_data['cst'] = charger_data['tst']
                    #print(charger_data['cst'])

                """
                print(charger_data['sid'])
                print(charger_data['chgeMange'])
                print(charger_data['snm'])
                print(charger_data['cid'])
                print(charger_data['ctp'])
                print(charger_data['cst'])
                print(charger_data['adr'])
                print(charger_data['x'])
                print(charger_data['y'])
                print(charger_data['utime'])
                """
                
                ctype_list=[]
                if int(charger_data['ctp']) == 1:
                    ctype_list.append(1)
                if int(charger_data['ctp']) == 2:
                    ctype_list.append(4)
                if int(charger_data['ctp']) == 3:
                    ctype_list.append(1)
                    ctype_list.append(3)
                if int(charger_data['ctp']) == 4:
                    ctype_list.append(2)
                if int(charger_data['ctp']) == 5:
                    ctype_list.append(2)
                    ctype_list.append(1)
                if int(charger_data['ctp']) == 6:
                    ctype_list.append(2)
                    ctype_list.append(1)
                    ctype_list.append(3)
                if int(charger_data['ctp']) == 7:
                    ctype_list.append(3)
                
                if not ctype_list[0]:
                    print("what!?!? the Fxxk ctype")
                
                station_ctype=[]
                for i in station.ctype.all():
                    
                    station_ctype.append(i.ctype)

                if station_ctype != ctype_list:
                    station_ctype=station_ctype+ctype_list
                    station_ctype=list(set(station_ctype))
                if station.edit == False:
                    station.ctype.set(station_ctype)

                    station.save()
                hash_charger = CidStat.objects.create(sidcid=charger_data['sid']+charger_data['cid'], cid=charger_data['cid'], stat=charger_data['cst'],sid=station)
                hash_charger.ctype.set(ctype_list)
                hash_charger.save()
                
                
            except:
                #print(charger_data['sid']+charger_data['cid'])
                try:
                    hash_ch=CidStat.objects.get(sidcid=charger_data['sid']+charger_data['cid'])
                    if charger_data['cst'] == 7:
                        charger_data['cst'] = charger_data['tst']
                    if hash_ch.stat != charger_data['cst']:
                        hash_charger = CidstatLog.objects.create(Cidstat=hash_ch, stat_before=hash_ch.stat, stat_after=charger_data['cst'])
                    hash_ch.stat=charger_data['cst']
                    hash_ch.save()
                    hash_ch.ctype.set(ctype_list)
                except:
                    pass
                    
        