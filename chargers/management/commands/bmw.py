from django.core.management.base import BaseCommand, CommandError
from django.contrib.gis.geos import Point
from chargers.models import Charger, CidStat, ChargerLog
from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime
from django.core import serializers
import requests
import string
import json
from time import sleep

class Command(BaseCommand):


    def handle(self, *args, **options):
        # -*- coding: utf-8 -*- 
        charger_count=0
        evwhereURL="https://evwhere.co.kr/api/chargingStation"
        evwhere_crawl = requests.get(evwhereURL)
        evwhere_data = evwhere_crawl.text
        evwhere_json = json.loads(evwhere_data)
        progress=0
        progress_percent=0
        a=[]
        for evwhere_data in evwhere_json['charging_station_list']:
            progress = progress+1
            progress_percent=str(int(100*(progress/len(evwhere_json['charging_station_list']))))
            progress_count="("+str(progress)+"/"+str(len(evwhere_json['charging_station_list']))+")"
            print("evwhere " + progress_percent+"%"+progress_count)
            cid=0
            sid = "5000"+str(evwhere_data['id'])
            try:
                charger = Charger.objects.get(sid=sid)
                if 16 == evwhere_data['charging_service_provider_id']:  
                    charger.bid.set(['BM'])
                    charger.ctype.set([4])
                    charger.pay.set([1])


            except:
                if 16 == evwhere_data['charging_service_provider_id']:
                    try:
                        
                        chargerURL="https://evwhere.co.kr/api/chargingStation/"+str(evwhere_data['id'])
                        charger_crawl = requests.get(chargerURL)
                        charger_json = json.loads(charger_crawl.text)
                        if "인천 중구 운서동 1677-77" in charger_json['charging_station_list'][0]['land_lot_number_address']:
                            url = "https://dapi.kakao.com/v2/local/search/address.json?query="+charger_json['charging_station_list'][0]['land_lot_number_address']
                            location_data=requests.get(url ,headers={'Authorization': 'KakaoAK b3fc8bbabf0c386c96be72eb5b270a4a'})                    
                            location_json = json.loads(location_data.text)
                            point=Point(float(location_json['documents'][0]['x']),float(location_json['documents'][0]['y']))
                            region1=location_json['documents'][0]['address']['region_1depth_name']
                            region2=location_json['documents'][0]['address']['region_2depth_name']
                            print(charger_json['charging_station_list'][0]['road_name_address'],location_json['documents'][0]['address']['region_1depth_name'],location_json['documents'][0]['address']['region_2depth_name'])
                        
                        charger = Charger.objects.create(sid=sid, name = charger_json['charging_station_list'][0]['name'], addrDoro=charger_json['charging_station_list'][0]['road_name_address'], region1=location_json['documents'][0]['address']['region_1depth_name'], region2=location_json['documents'][0]['address']['region_2depth_name'], lat=location_json['documents'][0]['y'], lng=location_json['documents'][0]['x'], useTime='', LastUsedTime="",  stat="0", point=point)

                        url = "https://dapi.kakao.com/v2/local/search/address.json?query="+charger_json['charging_station_list'][0]['road_name_address']
                        location_data=requests.get(url ,headers={'Authorization': 'KakaoAK b3fc8bbabf0c386c96be72eb5b270a4a'})                    
                        location_json = json.loads(location_data.text)
                        point=Point(float(location_json['documents'][0]['x']),float(location_json['documents'][0]['y']))
                        region1=location_json['documents'][0]['address']['region_1depth_name']
                        region2=location_json['documents'][0]['address']['region_2depth_name']
                        print(charger_json['charging_station_list'][0]['road_name_address'],location_json['documents'][0]['address']['region_1depth_name'],location_json['documents'][0]['address']['region_2depth_name'])
                        
                        charger = Charger.objects.create(sid=sid, name = charger_json['charging_station_list'][0]['name'], addrDoro=charger_json['charging_station_list'][0]['road_name_address'], region1=location_json['documents'][0]['address']['region_1depth_name'], region2=location_json['documents'][0]['address']['region_2depth_name'], lat=location_json['documents'][0]['y'], lng=location_json['documents'][0]['x'], useTime='', LastUsedTime="",  stat="0", point=point)
                        print(charger.addrDoro, charger.region1, charger.region2)
                        print()
                    except:
                        pass
                    
            if 16 == evwhere_data['charging_service_provider_id']:
                chargerURL="https://evwhere.co.kr/api/chargingStation/"+str(evwhere_data['id'])
                try:
                    charger_crawl = requests.get(chargerURL)
                    charger_json = json.loads(charger_crawl.text)
                    cid=1
                    for i in charger_json['charging_station_list'][0]['charger_list']:
                        sidcid=sid+str(cid)
                        try:
                            CidStat.objects.create(sidcid=sidcid, cid=str(cid), stat=0,sid=charger)
                            CidStat.ctype.set([4])
                            cid+=1
                        except:
                            pass
                except:
                    pass

            