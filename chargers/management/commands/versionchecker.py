from django.core.management.base import BaseCommand, CommandError
from django.contrib.gis.geos import Point
from chargers.models import Charger, CidStat, ChargerLog, VersionCheck
from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime
from django.core import serializers
import requests
import string
import json
import os
from time import sleep

class Command(BaseCommand):
    def handle(self, *args, **options):
        try:
            
            tesla_charger_crawl=os.popen('curl \'https://play.google.com/apps/publish/appreleases?account=6499018101936990880&hl=ko\' -H \'cookie: _ga=GA1.3-3.1134451630.1531971388; _gid=GA1.3-3.195832122.1531971388; SID=QgY5Tbyps3SK8kP-LeO4sQMol4dZMgFO14VoViIOJaXOT3hKS2C7_DS04PEUXiZTt6epSg.; HSID=AxzsBlHzGCBswOCJ8; SSID=ANi02FVdcPRhQn2fk; APISID=Zs55kAd-l95GOCdL/AUAKWKHpWlAK7kZYf; SAPISID=dAf_NQcWFSWAfEad/Au5zffQzjsMZN7T0K; CONSENT=YES+KR.ko+20170611-09-0; enabledapps.uploader=0; NID=134=jmh2IbIJhpZtVPoKJYENdLyco-J0TqhuuYQsi68ung1VD6nRRLrTqVB4_pjNySgWTp4hvdkD1lav7ZxHvnSiSgPu8ZN_bzMLzBXo5v6Gg1ag0SdSIh88k7HyOOemb5GolJ9CtaFHTJMBL-VWS1UfESFBfrhY_B_Cbx_DLkg5; SIDCC=AEfoLebg9W7wToRbiG-yZSMpxkzezedK8ktL3iKGVRn1XIwTkN1oQsgdWi7Rvcv6BekqEGEm\' -H \'origin: https://play.google.com\' -H \'accept-encoding: gzip, deflate, br\' -H \'accept-language: ko\' -H \'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36\' -H \'content-type: application/javascript; charset=UTF-8\' -H \'accept: */*\' -H \'x-gwt-module-base: https://ssl.gstatic.com/play-apps-publisher-rapid/fox/105b159394dcc7a403f54ae82b3e45f9/fox/gwt/\' -H \'x-gwt-permutation: B2A841A0D30CE720C7363955E3AA0CA0\' -H \'authority: play.google.com\' -H \'referer: https://play.google.com/apps/publish/?hl=ko&account=6499018101936990880\' --data-binary \'{"method":"getReleaseTrackReleases","params":"{\\"1\\":\\"4697951745332313295\\"}","xsrf":"AMtNNDGMa5c84I6h-8G9o9EAbudwMdaNrQ:1531978314867"}\' --compressed').read()
            tesla_charger_json = json.loads(tesla_charger_crawl)
                
            try:
                VersionCheck.objects.get(version=tesla_charger_json['result']['5'][0]['4']['1'][0]['7']+" "+tesla_charger_json['result']['5'][0]['4']['1'][0]['6'])
            except:
                VersionCheck.objects.create(version=tesla_charger_json['result']['5'][0]['4']['1'][0]['7']+" "+tesla_charger_json['result']['5'][0]['4']['1'][0]['6'])

        except:
            headers = {'x-waple-authorization': 'Nzc3OS0xNTIwNDA5NDM4NzM5LTI5NmVlNGQxLWMwYTItNDk5NC1hZWU0LWQxYzBhMjQ5OTQ0Mw=='}
            r = requests.post('http://api.apistore.co.kr/ppurio/1/message/sms/evon', data={'dest_phone':'01064193155','send_phone':'07043530937','id':'evon', 'msg_body':'버전체커에 오류가 생겼습니다.'},headers=headers)
            r = requests.post('http://api.apistore.co.kr/ppurio/1/message/sms/evon', data={'dest_phone':'01050413445','send_phone':'07043530937','id':'evon', 'msg_body':'버전체커에 오류가 생겼습니다.'},headers=headers)
            
            exit()
                