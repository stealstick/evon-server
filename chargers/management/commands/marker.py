
from django.core.management.base import BaseCommand, CommandError
from django.core import serializers
import string
import json
import os

class Command(BaseCommand):
    def handle(self, *args, **options):

        path_dir='templates/marker'
        markers = os.listdir(path_dir)
        markers.sort()

        for marker in markers:
            f = open(path_dir+"/"+marker, 'r')
            
            if marker == ".DS_Store":
                continue
            
            data = f.read()
            start=data.find("#fff")
            front_data=data[0:start+8]
            end_data=data[start+8:]
            path_bid_dir='static/bid'
            bids = os.listdir(path_bid_dir)
            bids.sort()
            for bid in bids:
                if bid == ".DS_Store":
                    continue
                bid_f = open(path_bid_dir+"/"+bid, 'r')
                bid_data = bid_f.read()
                bidmarker = front_data+bid_data+end_data
                markername=marker.split('.')[0]
                f1 = open('templates/bidmarker'+"/"+markername+"-"+bid, 'w')
                f1.write(bidmarker)
            print(marker)