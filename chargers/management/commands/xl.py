from django.core.management.base import BaseCommand, CommandError
from django.contrib.gis.geos import Point
from chargers.models import Charger, CidStat, ChargerLog, CidstatLog
from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime
from django.core import serializers
import requests
import string
import json
import os
from time import sleep
import openpyxl

class Command(BaseCommand):


    def handle(self, *args, **options):
        # -*- coding: utf-8 -*- 
        wb = openpyxl.load_workbook('log.xlsx')
        ws = wb.active
        logs = CidstatLog.objects.all()
        row=1
        for log in logs:
            ws.cell(row=row, column=1).value = row
            ws.cell(row=row, column=2).value = log.Cidstat.sid.name
            ws.cell(row=row, column=3).value = log.Cidstat.sid.sid
            ws.cell(row=row, column=4).value = log.Cidstat.cid
            ws.cell(row=row, column=5).value = log.stat_before
            ws.cell(row=row, column=6).value = log.stat_after
            ws.cell(row=row, column=7).value = log.time
            row+=1
        wb.save("log2.xlsx")
        wb.close()