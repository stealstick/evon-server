
from django.core.management.base import BaseCommand, CommandError
from chargers.models import Charger, Region
from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime
from django.core import serializers
import requests
import string
import json
from time import sleep

class Command(BaseCommand):

    def handle(self, *args, **options):
        queryset=Charger.objects.all()
        region1_count={}
        region2_count={}
        region_data1=[]
        region_data2=[]
        for charger in queryset:
            count = region1_count.get(charger.region1,0)
            region1_count[charger.region1] = count + 1
        for region_a1 in region1_count:
            region_data1.append(region_a1)
            url = "https://dapi.kakao.com/v2/local/search/address.json?query="+region_a1
            location_data=requests.get(url ,headers={'Authorization': 'KakaoAK b3fc8bbabf0c386c96be72eb5b270a4a'})
            location_json = json.loads(location_data.text)
            region1_x=location_json['documents'][0]['x']
            region1_y=location_json['documents'][0]['y']
            try:
                Region.objects.create(depth=1, region=region_a1,lng=region1_x, lat=region1_y)
                print("success")
            except:
                print("already")
            #print(region_a1)


        for charger in queryset:
            i = charger.region1+" "+charger.region2
            if charger.region1=="세종특별자치시":
                i= "세종특별자치시 세종"
            count = region2_count.get(i,0)
            region2_count[i] = count + 1
        for region_a2 in region2_count:
            url = "https://dapi.kakao.com/v2/local/search/address.json?query="+region_a2
            location_data=requests.get(url ,headers={'Authorization': 'KakaoAK b3fc8bbabf0c386c96be72eb5b270a4a'})
            location_json = json.loads(location_data.text)
            region2_x=location_json['documents'][0]['x']
            region2_y=location_json['documents'][0]['y']
            try:
                Region.objects.create(depth=2, region=region_a2,lng=region2_x, lat=region2_y)
                print("success")
            except:
                print("already")
            



