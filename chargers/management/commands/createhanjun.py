from django.core.management.base import BaseCommand, CommandError
from django.contrib.gis.geos import Point
from chargers.models import Charger, CidStat, ChargerLog, CidstatLog
from django.shortcuts import render
from django.http import HttpResponse
from django.core import serializers
from django.core.files.base import ContentFile
from urllib.request import urlopen
from datetime import datetime
from bs4 import BeautifulSoup
import requests
import string
import json
import ssl
import time

from time import sleep

    

class Command(BaseCommand):
    def handle(self, *args, **options):
        # -*- coding: utf-8 -*- 
        chargerURL="https://evc.kepco.co.kr:4445/map/mapList.do"
        params={"searchDiv":1,"cpStat0":"","cpStat1":1,"cpStat2":2,"cpStat3":3,"cpStat4":4,"cpStat5":5,"cpStat6":1,"cpStat7":2,"cpStat8":"","cpStat9":"","lat":"","longi":"","highway":"","mapSeaDiv":"","all":"","sCnt":"","eCnt":"","sido":"","gugun":"","addr":"","car":"","sName":"","eName":"","startX":"","startY":"","endX":"","endY":"","cpTp":[0,1,2,3,4]}
        charger_crawl = requests.post(chargerURL, params=params)
        
        chargers_text=charger_crawl.text
        charger_list = chargers_text.split('map_idx=map_idx+1;')
        chargers=[]
        progress=0
        time_total=0
        progress_time=[]
        progress_percent=0
        count=0
        print()
        for i in range(len(charger_list)-1):
            start_time = time.time()
            progress = progress+1
            progress_percent=str(int(100*(progress/(len(charger_list)-1))))
            progress_count="("+str(progress)+"/"+str(len(charger_list)-1)+")"
            

            station={}
            # -- 고유 id 추출 --
            find_map_csNm = charger_list[i].find('parent.map_csId[map_idx] = "')
            start_map_csNm=find_map_csNm+len('parent.map_csId[map_idx] = "')
            end_map_csNm=start_map_csNm+charger_list[i][start_map_csNm:start_map_csNm+100].find('";')
            sid = charger_list[i][start_map_csNm:end_map_csNm]
            station['sid']=sid
            # -- 위도 추출 --
            find_map_csNm = charger_list[i].find('parent.map_lat[map_idx] = "')
            start_map_csNm=find_map_csNm+len('parent.map_lat[map_idx] = "')
            end_map_csNm=start_map_csNm+charger_list[i][start_map_csNm:start_map_csNm+100].find('";')
            lat = charger_list[i][start_map_csNm:end_map_csNm]
            station['lat']=lat
            # -- 경도 추출 --
            find_map_csNm = charger_list[i].find('parent.map_longi[map_idx] = "')
            start_map_csNm=find_map_csNm+len('parent.map_longi[map_idx] = "')
            end_map_csNm=start_map_csNm+charger_list[i][start_map_csNm:start_map_csNm+100].find('";')
            lng = charger_list[i][start_map_csNm:end_map_csNm]
            station['lng']=lng

            # -- 주소 추출 --
            find_map_csNm = charger_list[i].find('parent.map_addr[map_idx] = "')
            start_map_csNm=find_map_csNm+len('parent.map_addr[map_idx] = "')
            end_map_csNm=start_map_csNm+charger_list[i][start_map_csNm:start_map_csNm+100].find('";')
            addr = charger_list[i][start_map_csNm:end_map_csNm]
            station['addr']=addr
            
            try:
                hash_charger = Charger.objects.get(sid="8000"+str(station['sid']))
            except:
                try:
                    print('kakko')
                    url = "https://dapi.kakao.com/v2/local/geo/coord2regioncode.json?x="+station['lng']+"&y="+station['lat']
                    location_data=requests.get(url ,headers={'Authorization': 'KakaoAK b3fc8bbabf0c386c96be72eb5b270a4a'})
                    location_json = json.loads(location_data.text)
                    addr=region2=location_json['documents'][0]['address_name']
                    region1=location_json['documents'][0]['region_1depth_name']
                    region2=location_json['documents'][0]['region_2depth_name']
                    station['region1']=region1
                    station['region2']=region2c
                except:
                    region1=''
                    region2=''
                    station['region1']=region1
                    station['region2']=region2
            # -- 충전소명 추출 --
            find_map_csNm = charger_list[i].find('parent.map_csNm[map_idx] = "')
            start_map_csNm=find_map_csNm+len('parent.map_csNm[map_idx] = "')
            end_map_csNm=start_map_csNm+charger_list[i][start_map_csNm:start_map_csNm+100].find('";')
            name = charger_list[i][start_map_csNm:end_map_csNm]
            station['name']=name
            # -- 완속 추출 --
            find_map_csNm = charger_list[i].find('parent.map_charcnt1[map_idx] = "')
            start_map_csNm=find_map_csNm+len('parent.map_charcnt1[map_idx] = "')
            end_map_csNm=start_map_csNm+charger_list[i][start_map_csNm:start_map_csNm+100].find('";')
            slow = charger_list[i][start_map_csNm:end_map_csNm]
            station['slow']=slow

            # -- 급속 추출 --
            find_map_csNm = charger_list[i].find('parent.map_charcnt2[map_idx] = "')
            start_map_csNm=find_map_csNm+len('parent.map_charcnt2[map_idx] = "')
            end_map_csNm=start_map_csNm+charger_list[i][start_map_csNm:start_map_csNm+100].find('";')
            fast = charger_list[i][start_map_csNm:end_map_csNm]
            station['fast']=fast

            # -- 사용가능 충전 추출 --
            find_map_csNm = charger_list[i].find('parent.map_cnt1[map_idx] = "')
            start_map_csNm=find_map_csNm+len('parent.map_cnt1[map_idx] = "')
            end_map_csNm=start_map_csNm+charger_list[i][start_map_csNm:start_map_csNm+100].find('";')
            useable = charger_list[i][start_map_csNm:end_map_csNm]
            if useable is not 0:
                useable=2
            
            else:
                useable=3
            station['useable']=useable

            

            #충전소 리스트 추가
            chargers.append(station)

            #충전소 생성
            
            point=Point(float(station['lng']),float(station['lat']))
            try:
                if("노상 공영" in station['name']):
                    sleep(10)
                hash_charger = Charger.objects.create(sid="8000"+str(station['sid']), name = station['name'], addrDoro=station['addr'], region1=station['region1'], region2=station['region2'], lat=station['lat'], lng=station['lng'], useTime='', stat=station['useable'], point=point)
                hash_charger.pay.set([1])
                hash_charger.bid.set(['KP'])
            except Exception as e:
                hash_charger = Charger.objects.get(sid="8000"+str(station['sid']))
                #print(station['name']+" created")
                #print(str(e))
                count+=1
                pass
            

            chargerURL="https://evc.kepco.co.kr:4445/map/popup2.do?type=map&csId="+str(station['sid'])
            charger_crawl = requests.get(chargerURL)
            charger_html=charger_crawl.text
            soup = BeautifulSoup(charger_html, "html.parser")
            # #-----충전소 사진    
            # if hash_charger.charger_img1 == "defalutChargerImg.jpg":

            # 	find_img_st = charger_html.find('if("EVCP')
            # 	if find_img_st == -1:
            # 	    find_img_st = charger_html.find('if("FILE')
            # 	find_img_end= charger_html.find('" == null ||')
            # 	img_name=charger_html[find_img_st+4:find_img_end]
            # 	if img_name != '':
            # 	    imgURL="https://evc.kepco.co.kr:4445/map/image.do?atchFileId="+img_name
            # 	    img_crawl = requests.get(imgURL)
            # 	    img_html = img_crawl.text
            # 	    soup = BeautifulSoup(img_html, "html.parser")
            # 	    hanjun_imgs=[]
            # 	    for i in range(1,10):
            # 	        hanjun_img=soup.select("a:nth-of-type("+str(i)+") > img")
            # 	        if hanjun_img ==[]:
            # 	            break
            # 	        hanjun_imgs.append(hanjun_img[0]['src'])
            # 	    try:
            # 	        img_url="https://evc.kepco.co.kr:4445"+hanjun_imgs[0]
            # 	        context = ssl._create_unverified_context()
            # 	        img01=urlopen(img_url,context=context)
            # 	        hash_charger.charger_img1.save('a.jpg', ContentFile(img01.read()))
            # 	        hash_charger.save()
            # 	        img_url="https://evc.kepco.co.kr:4445"+hanjun_imgs[1]
            # 	        context = ssl._create_unverified_context()
            # 	        img02=urlopen(img_url,context=context)
            # 	        hash_charger.charger_img2.save('a.jpg', ContentFile(img02.read()))
            # 	        hash_charger.save()
            # 	    except:
            # 	        pass
            
            chargerURL="https://evc.kepco.co.kr:4445/map/popup2.do?type=map&csId="+str(station['sid'])

            

            charger_crawl = requests.get(chargerURL)
            charger_html=charger_crawl.text
            soup = BeautifulSoup(charger_html, "html.parser")
            #----------ㅈㅓㄴ화번호
            charger_tel=soup.select("#pop_tab01 > div > table > tbody > tr:nth-of-type(6) > td")
            tel=str(charger_tel[0].text.strip())
            #----------주차비
            charger_parking=soup.select("#pop_tab01 > div > table > tbody > tr:nth-of-type(5) > td")
            parking=str(charger_parking[0].text.strip())
            #----------상세위치
            charger_detail_locate=soup.select("#pop_tab01 > div > table > tbody > tr:nth-of-type(2) > td")
            detail_locate=str(charger_detail_locate[0].text.strip())
            #----------
            charger_use_time=soup.select("#pop_tab01 > div > table > tbody > tr:nth-of-type(4) > td")
            use_time=str(charger_use_time[0].text.strip())


            chargers=[]
            charger_ctype=[]
            if parking:
                note="주차비 : "+parking
            if detail_locate:
                detail_addr=detail_locate
            
            print(note)
            #----충전기 각각------
            for i in range(1,30):
                charger=soup.select("body > div.scroll_area > div > div > table > tbody > tr:nth-of-type("+str(i)+")")
                if charger==[]:
                    break

                if "급속" in str(charger[0]):
                    ctype_list=[1, 2, 3]
                    charger_ctype.append(1)
                    charger_ctype.append(2)
                    charger_ctype.append(3)
                elif "완속" in str(charger[0]):
                    ctype_list=[4]
                    charger_ctype.append(4)
                else:
                    break

                if "충전가능" in str(charger[0]):
                    stat=2
                elif "충전중" in str(charger[0]):
                    stat=3
                elif "고장" in str(charger[0]):
                    stat=5
                    print(station['name']+" create")
                    try:
                        fix_about=soup.select("body > div.scroll_area > div > div> table > tbody > tr:nth-of-type(2) > td:nth-of-type(1)")
                        if "고장 안내" in fix_about:
                            charger_note=soup.select("body > div.scroll_area > div > div > table > tbody > tr:nth-of-type(2) > td:nth-of-type(2)")
                            note = note+"\n"+charger_note[0].text.strip()
                    except:
                        pass
                elif "통신장애" in str(charger[0]):
                    stat=1
                else:
                    stat=0
                hash_charger=Charger.objects.get(sid="8000"+str(station['sid']))
                sid="8000"+str(station['sid'])+"0"+str(i)
                try:
                    charger_con = CidStat.objects.create(sidcid=sid, cid=i, stat=stat,sid=hash_charger)
                    stat_old=10
                except:
                    charger_con = CidStat.objects.get(sidcid=sid)
                    stat_old=charger_con.stat
                charger_con.ctype.set(ctype_list)
                charger_con.stat=stat
                if stat_old!=10 and stat_old!=stat:
                    cidlog = CidstatLog.objects.create(Cidstat=charger_con, stat_before=stat_old, stat_after=stat)
                if hash_charger.edit == False:
                    hash_charger.ctype.set(charger_ctype)
                    hash_charger.tel=tel
                    hash_charger.pay.set([1])
                    hash_charger.bid.set(['KP'])
                    hash_charger.note=note
                    hash_charger.detail_addr=detail_addr
                hash_charger.save()
                charger_con.save()
            print("hanjun " + progress_percent+"%"+progress_count)
            progress_time.append(time.time() - start_time)
            for j in progress_time:
                time_total=time_total+j
            
            minute=((len(charger_list)-1-progress) * time_total/progress)/60
            second=((len(charger_list)-1-progress) * time_total/progress)%60
            print("Estimated remaining time %02d : %02d" %(minute, second ))
            time_total=0
        
            

            