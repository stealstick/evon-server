from django.core.management.base import BaseCommand, CommandError
from django.contrib.gis.geos import Point
from chargers.models import Charger, CidStat, ChargerLog
from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime
from django.core import serializers
import requests
import string
import json
import os
from time import sleep

class Command(BaseCommand):


    def handle(self, *args, **options):
        # -*- coding: utf-8 -*- 
        charger_count=0
        teslaURL="https://www.tesla.com/ko_KR/findus#/bounds/37.6956,127.18229999999994,37.4346,126.79680000000008,d?search=supercharger,store,service,destinationcharger,"
        tesla_crawl = requests.get(teslaURL)
        tesla_data_start = tesla_crawl.text.find('location_data')+16
        tesla_data_end = tesla_crawl.text.find(';;')
        tesla_data = tesla_crawl.text[tesla_data_start:tesla_data_end]
        tesla_json = json.loads(tesla_data)

        for tesla_charger in tesla_json:
            if 32.92978608013124<float(tesla_charger['latitude']) and float(tesla_charger['latitude'])<38.97172906083053 and float(tesla_charger['longitude'])>125.08491819415008 and float(tesla_charger['longitude'])<130.75386350665008:
                tesla_charger_URL="curl -s 'https://www.tesla.com/ko_KR/api?m=findus_map&a=location&place="+tesla_charger['location_id']+"'"
                tesla_charger_crawl=os.popen(tesla_charger_URL).read()
                tesla_charger_json = json.loads(tesla_charger_crawl)
                try:
                    hash_ch=Charger.objects.get(sid=tesla_charger_json['nid'])
                    bid=['TE']
                    hash_ch.bid.set(bid)

                    many_charger= tesla_charger_json['chargers'][count_charger]
                    for i in range(int(many_charger)):
                            print(tesla_charger_json['nid']+"0"+str(i+1))

                            chargers = CidStat.objects.get(sidcid=tesla_charger_json['nid']+"0"+str(i+1))
                            print(chargers.sid.name)
                            chargers.ctype.set([7])
                            print(chargers.ctype.all())

                except:
                    try:
                        tesla_charger_json = json.loads(tesla_charger_crawl)
                        url = "https://dapi.kakao.com/v2/local/geo/coord2regioncode.json?x="+tesla_charger_json['longitude']+"&y="+tesla_charger_json['latitude']
                        location_data=requests.get(url ,headers={'Authorization': 'KakaoAK b3fc8bbabf0c386c96be72eb5b270a4a'})
                        location_json = json.loads(location_data.text)
                        addr=region2=location_json['documents'][0]['address_name']
                        charger_count+=1
                        count_charger=tesla_charger_json['chargers'].index('개')-1
                        many_charger= tesla_charger_json['chargers'][count_charger]
                        region1=location_json['documents'][0]['region_1depth_name']
                        region2=location_json['documents'][0]['region_2depth_name']
                        point=Point(float(tesla_charger_json['longitude']),float(tesla_charger_json['latitude']))
                        ctype_list=[7]
                        try:
                            usetime=tesla_charger_json['hours'].replace('<p><strong>운영시간</strong><br/>','')
                        except:
                            usetime=""
                        hash_charger = Charger.objects.create(sid=tesla_charger_json['nid'], name = tesla_charger_json['title'], addrDoro=tesla_charger_json['address'], region1=region1, region2=region2, lat=tesla_charger_json['latitude'], lng=tesla_charger_json['longitude'], useTime=usetime, LastUsedTime="09:00", stat=0, point=point)
                        hash_charger.bid.set(['TE'])
                        hash_charger.pay.set([0])
                        print("suc")
                        hash_charger.ctype.set(ctype_list)
                        for i in range(int(many_charger)):
                            print(many_charger,i)
                            chargers = CidStat.objects.create(sidcid=tesla_charger_json['nid']+"0"+str(i+1), cid=str(i+1), stat=0,sid=hash_charger)
                            chargers.Ctype.set([7])
                            print("suc"+str(i+1))
                    
                    except:
                        pass

                    
                #hash_ch.ctype=ctype_list
            else:
                pass
