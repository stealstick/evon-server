from django.core.management.base import BaseCommand, CommandError
from chargers.models import Charger, CidStat
class Command(BaseCommand):
    def handle(self, *args, **options):
        charger_cidstat = {}
        charger_stat = {}
        cidstat = CidStat.objects.all()
        for cid in cidstat:
            try:
                charger_cidstat[cid.sid.sid].append(cid.stat)
            except:
                charger_cidstat[cid.sid.sid]=[]
                charger_cidstat[cid.sid.sid].append(cid.stat)

        
        for i in charger_cidstat:
            stat=0
            if 4 in charger_cidstat[i]:
                stat=4
            if 5 in charger_cidstat[i]:
                stat=5
            if 6 in charger_cidstat[i]:
                stat=6
            if 1 in charger_cidstat[i]:
                stat=1
            if 0 in charger_cidstat[i]:
                stat=0
            if 9 in charger_cidstat[i]:
                stat=9
            if 7 in charger_cidstat[i]:
                stat=7
            if 3 in charger_cidstat[i]:
                stat=3
            if 2 in charger_cidstat[i]:
                stat=2
            charger_stat[i]=stat
            charger=Charger.objects.get(sid=i)
            charger.stat=stat
            charger.save()

