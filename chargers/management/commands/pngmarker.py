
from django.core.management.base import BaseCommand, CommandError
from django.core import serializers
import string
import json
import os
import cairosvg
import os, sys
class Command(BaseCommand):
    def handle(self, *args, **options):
        path_dir='templates/bidmarker'

        markers = os.listdir(path_dir)
        markers.sort()
        for marker in markers:
            if "obstacle" in marker:
                stats=[0,1,7,9]
            elif "possibility" in marker:
                stats=[2]
            elif "ing" in marker:
                stats=[3]
            elif "stop" in marker:
                stats=[4]
            elif "check" in marker:
                stats=[5]
            else:
                continue
            for stat in stats:
                bid=marker[-6:-4]
                savepath="static/marker/"+bid.upper()+"/"+str(stat)+".png"
                cairosvg.svg2png(url=path_dir+"/"+marker, write_to=savepath)


        

        


    #         if stat==0 or stat==1 or stat==7 or stat==9:
    #     stat="obstacle"
    # elif stat==2:
    #     stat="possibility"
    # elif stat==3:
    #     stat="ing"
    # elif stat==4:
    #     stat="stop"
    # elif stat==5:
    #     stat="check"