from django.core.management.base import BaseCommand, CommandError
from django.contrib.gis.geos import Point
from chargers.models import Charger, CidStat, ChargerLog, CidstatLog, ChargersNear
from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime
from django.core import serializers
from django.core.files.base import ContentFile
from urllib.request import urlopen
from io import BytesIO
import requests
import string
import json
from PIL import Image
from time import sleep

class Command(BaseCommand):


    def handle(self, *args, **options):
        # -*- coding: utf-8 -*- 
        chargers = Charger.objects.all()
        ChargersNear.objects.create(sid=chargers[0],place_name='ㄴ',address_name='ㄴ',road_address_name='ㄴ',category_group_code='CS2')
        i=0
        categorys=['PK6','CE7','CT1','AT4','MT1','FD6','BK9','CS2']
        for charger in chargers:
            i+=1
            print("%dper (%d/%d)" % (int(i/len(chargers)*100), i, len(chargers)))
            for category in categorys:
                url = "https://dapi.kakao.com/v2/local/search/category.json?category_group_code="+category+"&x="+str(charger.lng)+"&y="+str(charger.lat)+"&radius=100"
                location_data=requests.get(url ,headers={'Authorization': 'KakaoAK b3fc8bbabf0c386c96be72eb5b270a4a'})
                location_json = json.loads(location_data.text)
             
                for location in location_json['documents']:
                    if 0 == len(ChargersNear.objects.filter(sid=charger,place_name=location['place_name'])):
                        ChargersNear.objects.create(sid=charger,place_name=location['place_name'],address_name=location['address_name'],road_address_name=location['road_address_name'],category_group_code=location['category_group_code'], lat= location['y'], lng=location['x'])
                    else:
                        continue