from django.core.management.base import BaseCommand, CommandError
from django.contrib.gis.geos import Point
from chargers.models import Charger, CidStat, ChargerLog
from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime
from django.core import serializers
import requests
import string
import json
from time import sleep

class Command(BaseCommand):


    def handle(self, *args, **options):
        # -*- coding: utf-8 -*- 
        charger_count=0
        koreagmURL="https://www.ev-line.co.kr/charge/mapdataadd.asp"
        koreagm_crawl = requests.get(koreagmURL)   
        f = open("chargers/data/gm.json", 'r')
        koreagm_file = f.read()
        koreagm_json = json.loads(koreagm_file)
        progress=0
        progress_percent=0

        for koreagm_charger in koreagm_json:
            for koreagm_darijum in koreagm_json[koreagm_charger]:
                try:
                    progress+=1
                    sid=6000000+progress
                    hash_ch=Charger.objects.get(sid=sid)
                    bid=['GM']
                    pay=[1]
                    hash_ch.bid.set(bid)
                    hash_ch.pay.set(pay)
                    hash_ch.addrDoro=koreagm_data[4]
                    cid_count=1
                    try:
                        for i in range(int(koreagm_data[6])):
                            charger_count+=1
                            cid_charger = CidStat.objects.get(sidcid=str(sid)+"0"+str(cid_count))
                            cid_count+=1
                            
                    except:
                        pass
                    try:
                        for i in range(int(koreagm_data[5])):
                            charger_count+=1
                            cid_charger = CidStat.objects.get(sidcid=str(sid)+"0"+str(cid_count))
                            cid_count+=1
                            cid_charger.ctype.set(ctype_list)
                    except:
                        pass
                    
                except:
                    try:
                        
                        koreagm_data=koreagm_json[koreagm_charger][koreagm_darijum]
                        
                        url = "https://dapi.kakao.com/v2/local/search/address.json?query="+koreagm_data[4]
                        
                        location_data=requests.get(url ,headers={'Authorization': 'KakaoAK b3fc8bbabf0c386c96be72eb5b270a4a'})
                        
                        location_json = json.loads(location_data.text)
                        print(location_json)
                        region_x=location_json['documents'][0]['x']
                        region_y=location_json['documents'][0]['y']

                        
                        point=Point(float(region_x),float(region_y))
                        url = "https://dapi.kakao.com/v2/local/geo/coord2regioncode.json?x="+region_x+"&y="+region_y
                        location_data=requests.get(url ,headers={'Authorization': 'KakaoAK b3fc8bbabf0c386c96be72eb5b270a4a'})
                        location_json = json.loads(location_data.text)
                        addr=region2=location_json['documents'][0]['address_name']
                        region1=location_json['documents'][0]['region_1depth_name']
                        region2=location_json['documents'][0]['region_2depth_name']
                        ctype_list=[4]
                        
                        name="쉐보레 "+koreagm_data[2]+"전시장"
                        
                        hash_charger = Charger.objects.create(sid=sid, name = name, addrDoro=koreagm_data[4],tel=koreagm_data[3], region1=region1, region2=region2, lat=region_x, lng=region_y, useTime="24시간 이용가능", LastUsedTime="09:00", stat=9, point=point)
                        hash_charger.bid.set(['GM'])
                        hash_charger.ctype.set(ctype_list)
                        cid_count=1
                        for i in range(int(koreagm_data[5])):
                            cid_charger = CidStat.objects.create(sidcid=str(sid)+"0"+str(cid_count), cid="0"+str(cid_count), stat=0,sid=hash_charger)
                            cid_count+=1
                            cid_charger.ctype.set(ctype_list)
                        for i in range(int(koreagm_data[6])):
                            cid_charger = CidStat.objects.create(sidcid=str(sid)+"0"+str(cid_count), cid="0"+str(cid_count), stat=0,sid=hash_charger)
                            cid_count+=1
                        
                    except:
                        print("err")             
                        pass
                    #hash_ch.ctype=ctype_list
                    #hash_ch.save()