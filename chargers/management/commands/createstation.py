from django.core.management.base import BaseCommand, CommandError
from django.contrib.gis.geos import Point
from chargers.models import Charger, CidStat, ChargerLog
from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime
from django.core import serializers
import requests
import string
import json
from time import sleep

class Command(BaseCommand):


    def handle(self, *args, **options):
        # -*- coding: utf-8 -*- 
        chargerURL="http://ev.or.kr/portal/monitor/chargerList"
        charger_crawl = requests.get(chargerURL)   
        chargers_data = charger_crawl.text
        chargers_json = json.loads(chargers_data)
        progress=0
        progress_percent=0
        chargers_list=[]
        for charger_data in chargers_json['chargerList']:
            #sleep(0.1)
            progress = progress+1
            progress_percent=str(int(100*(progress/len(chargers_json['chargerList']))))
            progress_count="("+str(progress)+"/"+str(len(chargers_json['chargerList']))+")"
            print("evorkr" + progress_percent+"%"+progress_count)
            chargers_list.append(charger_data['sid'])
            try:
                if charger_data['chgeMange']   == "11" or charger_data['chgeMange']   == 11:
                    bid=['BG']
                elif charger_data['chgeMange']   == "12" or charger_data['chgeMange']   == 12:
                    bid=['JE']
                elif charger_data['chgeMange']   == "13" or charger_data['chgeMange']   == 13:
                    bid=['KP']
                    hash_ch=Charger.objects.get(sid=charger_data['sid'])
                    hash_ch.delete()
                    continue
                elif charger_data['chgeMange']   == "14" or charger_data['chgeMange']   == 14:
                    bid=['HE']
                    continue
                elif charger_data['chgeMange']   == "15" or charger_data['chgeMange']   == 15:
                    bid=['PI']
                    hash_ch=Charger.objects.get(sid=charger_data['sid'])
                    hash_ch.delete()
                    continue
                elif charger_data['chgeMange']   == "16" or charger_data['chgeMange']   == 16:
                    bid=['SE']
                elif  charger_data['chgeMange']   == "17" or charger_data['chgeMange']   == 17:
                    bid=['GI']
                elif  charger_data['chgeMange']   == "18" or charger_data['chgeMange']   == 18:
                    bid=['HD']
                elif  charger_data['chgeMange']   == "19" or charger_data['chgeMange']   == 19:
                    bid=['JD']
                elif  charger_data['chgeMange']   == "20" or charger_data['chgeMange']   == 20:
                    bid=['DG']
                elif  charger_data['chgeMange']   == "22" or charger_data['chgeMange']   == 22:
                    bid=['SS']
                elif  charger_data['chgeMange']   == "23" or charger_data['chgeMange']   == 23:
                    bid=['ST']
                elif  charger_data['chgeMange']   == "24" or charger_data['chgeMange']   == 24:
                    bid=['GN']
                elif  charger_data['chgeMange']   == "25" or charger_data['chgeMange']   == 25:
                    bid=['KT']
                    continue
                elif  charger_data['chgeMange']   == "26" or charger_data['chgeMange']   == 26:
                    bid=['ER']
                    hash_ch=Charger.objects.get(sid=charger_data['sid'])
                    hash_ch.delete()
                    continue
                elif  charger_data['chgeMange']   == "27" or charger_data['chgeMange']   == 27:
                    bid=['WR']
                elif  charger_data['chgeMange']   == "28" or charger_data['chgeMange']   == 28:
                    bid=['IC']
                elif  charger_data['chgeMange']   == "29" or charger_data['chgeMange']   == 29:
                    bid=['SG']
                elif  charger_data['chgeMange']   == "30" or charger_data['chgeMange']   == 30:
                    bid=['DY']
                elif  charger_data['chgeMange']   == "31" or charger_data['chgeMange']   == 31:
                    bid=['CL']
                elif  charger_data['chgeMange']   == "31" or charger_data['chgeMange']   == 32:
                    bid=['EC']
                elif  charger_data['chgeMange']   == "89" or charger_data['chgeMange']   == 89: 
                    bid=['HO']
                elif  charger_data['chgeMange']   == "34" or charger_data['chgeMange']   == 34: 
                    bid=['BE']
                elif  charger_data['chgeMange']   == "" or charger_data['chgeMange']   == None: 
                    bid=['ME']
                else:
                    bid=['ME']
                
                if charger_data['cst'] is 7:
                    bid=['MT']
                    charger_data['cst'] = charger_data['tst']
                try:
                    hash_ch=Charger.objects.get(sid=charger_data['sid'])
                    hash_ch.bid.set(bid)
                except:
                    url = "https://dapi.kakao.com/v2/local/geo/coord2regioncode.json?x="+charger_data['y']+"&y="+charger_data['x']
                    location_data=requests.get(url ,headers={'Authorization': 'KakaoAK b3fc8bbabf0c386c96be72eb5b270a4a'})
                    location_json = json.loads(location_data.text)
                    region1=location_json['documents'][0]['region_1depth_name']

                    region2=location_json['documents'][0]['region_2depth_name']
                    addr=charger_data['adr']
                
                #----
                
                #region=addr.split()

                #region1=region[0]
                #region2=region[1]
                #if region1 is "세종특별자치시":
                #    region2= "세종특별자치시"
                
                #print(location_json)
                
                #hash_charger = Charger.objects.create(sid=charger_data['sid'], name = charger_data['snm'], addrDoro=addr, region1=region1, region2=region2, lat=charger_data['x'], lng=charger_data['y'], useTime=charger_data['utime'], LastUsedTime="09:00", bid=charger_data['chgeMange'],stat=charger_data['cst'])
                ctype_list=[]
                if  "안성휴게소" in charger_data['snm']:
                    print(charger_data['snm'])
                    sleep(1)
                if int(charger_data['ctp']) == 1:
                    ctype_list.append(1)
                if int(charger_data['ctp']) == 2:
                    ctype_list.append(4)
                if int(charger_data['ctp']) == 3:
                    ctype_list.append(1)
                    ctype_list.append(3)
                if int(charger_data['ctp']) == 4:
                    ctype_list.append(2)
                if int(charger_data['ctp']) == 5:
                    ctype_list.append(2)
                    ctype_list.append(1)
                if int(charger_data['ctp']) == 6:
                    ctype_list.append(2)
                    ctype_list.append(1)
                    ctype_list.append(3)
                if int(charger_data['ctp']) == 7:
                    ctype_list.append(3)
                if not ctype_list[0]:
                    print("what!?!? the Fxxk ctype")
                point=Point(float(charger_data['y']),float(charger_data['x']))
                hash_charger = Charger.objects.create(sid=charger_data['sid'], name = charger_data['snm'], addrDoro=addr, region1=region1, region2=region2, lat=charger_data['x'], lng=charger_data['y'], useTime=charger_data['utime'], LastUsedTime="09:00", stat=charger_data['cst'], point=point)
                hash_charger.ctype = ctype_list
                hash_charger.bid.set(bid)
                hash_charger.save
            except:
                try:
                    hash_ch=Charger.objects.get(sid=charger_data['sid'])
                    hash_ch.ctype.set(ctype_list)
                except:                
                    pass
                #hash_ch.ctype=ctype_list
        replace_station= Charger.objects.filter(bid="ME")
        """
        for i in replace_station:
            if i.sid in chargers_list:
                pass
            else:
                print(i.sid)
                hash_ch=Charger.objects.get(sid=i.sid)
                hash_ch.delete()
"""