import os
import time
import sys

from django.db import models
from PIL import Image
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.contrib.gis.db import models
from datetime import datetime

def get_upload_path_01(instance, filename):
    currently = time.strftime("%y%m%d%H%M%S")
    return os.path.join(
      "chargers" , "{}_{}.jpg".format(currently, instance.sid + "_01")
    )

def get_upload_path_02(instance, filename):
    currently = time.strftime("%y%m%d%H%M%S")
    return os.path.join(
      "chargers" , "{}_{}.jpg".format(currently, instance.sid + "_02")
    )

def get_upload_path_03(instance, filename):
    currently = time.strftime("%y%m%d%H%M%S")
    return os.path.join(
      "chargers" , "{}_{}.jpg".format(currently, instance.sid + "_03")
    )

def get_upload_path_04(instance, filename):
    currently = time.strftime("%y%m%d%H%M%S")
    return os.path.join(
      "chargers" , "{}_{}.jpg".format(currently, instance.sid + "_04")
    )

def get_upload_path_001(instance, filename):
    currently = time.strftime("%y%m%d%H%M%S")
    return os.path.join(
      "chargers" , "{}_{}.jpg".format(currently, instance.sid + "_001")
    )

def get_upload_path_002(instance, filename):
    currently = time.strftime("%y%m%d%H%M%S")
    return os.path.join(
      "chargers" , "{}_{}.jpg".format(currently, instance.sid + "_002")
    )

def get_upload_path_003(instance, filename):
    currently = time.strftime("%y%m%d%H%M%S")
    return os.path.join(
      "chargers" , "{}_{}.jpg".format(currently, instance.sid + "_003")
    )

def get_upload_path_004(instance, filename):
    currently = time.strftime("%y%m%d%H%M%S")
    return os.path.join(
      "chargers" , "{}_{}.jpg".format(currently, instance.sid + "_004")
    )
class Ctype(models.Model):
    CTYPE_CHOICES = (
        (1, 'DC차데모'),
        (2, 'DC콤보'),
        (3, 'AC상'),
        (4, '완속'),
        (5, 'B타입'),
        (6, 'C타입'),
        (7, '테슬라'),
    )
    ctype = models.PositiveIntegerField(choices=CTYPE_CHOICES, primary_key=True)
    def __unicode__(self):
        return self.ctype
    def __str__(self):
        return '<%s %s>' % (self.get_ctype_display(), str(self.ctype))

class Pay(models.Model):
    PAY = (
        (1, '유료'),
        (0, '무료'),
    )
    pay = models.PositiveIntegerField(choices=PAY, primary_key=True)
    def __unicode__(self):
        return self.pay
    def __str__(self):
        return '<%s %s>' % (self.get_pay_display(), str(self.pay))

class Bid(models.Model):
    BID =(
    	('BG', '비긴스'),
		('JE', '제주전기자동차서비스'),
		('KP', '한국전력'),
		('HE', '한국전기차충전서비스'),
		('PI', '포스코ICT'),
		('SE', '서울시'),
		('PW', '파워큐브'),
		('CT', '씨티카'),
		('ME', '환경부'),
		('JD', '제주특별자치도청'),
		('TM', '티맵'),
		('HM', '현대자동차네비게이션'),
		('GI', '기아자동차'),
		('HD', '현대자동차'),
		('DG', '대구환경공단'),
		('SS', '수원시'),
		('ST', '에스트래픽'),
		('GN', '지엔텔'),
		('KT', '케이티'),
		('ER', '에버온'),
		('WR', '울릉군청'),
		('HO', '수소충전소'),
		('IC', '인천국제공항'),
		('SG', '시그넷'),
		('DY', '대영채비'),
		('CL', '클린일렉스'),
		('MT', '환경부시범운영'),
        ('TE', '테슬라'),
        ('BM', 'BMW'),
        ('IK', '이케아'),
        ('EC', '이카플러그'),
        ('BE', '보리타에너지'),
        ('BZ', '벤츠'),
        ('EC', '기타'),
        ('JA', '중앙제어'),
    )
    BID_GROUP=(
    	(1, '전기차 제조업체'),
        (2, '정부 지자체'),
        (3, '전기차 충전 서비스 업체'),
        (4, '기타 서비스 업체'),
    )
    bid = models.CharField(max_length=10, primary_key=True)
    name =  models.CharField(max_length=50, null=True, blank=True)
    group = models.PositiveIntegerField(choices=BID_GROUP,  null=True, blank=True)
    def __unicode__(self):
        return self.bid
    def __str__(self):
        return '<%s %s>' % (self.name, str(self.bid))


class ChargerImage(models.Model):
    image=models.ImageField(upload_to=get_upload_path_001, default="defalutChargerImg.jpg")
    def __unicode__(self):
        return self.image


class Usetime(models.Model):
    weekday = models.CharField(max_length=500, default="", blank=True, help_text="평일")
    sat  = models.CharField(max_length=500, default="", blank=True, help_text="토요일")
    sun  = models.CharField(max_length=500, default="", blank=True, help_text="일요일")
    detail  = models.CharField(max_length=500, default="", blank=True, help_text="상세 정보")
    def __str__(self):
        charger = Charger.objects.filter(usetime=self)
        if len(charger) !=0:
            return '%s' % (charger[0].name)
        return '%s' % ("새로운 운영시간 데이터")

class ChargerParking(models.Model):
    base = models.CharField(max_length=500, default="", blank=True, help_text="기본 요금")
    add  = models.CharField(max_length=500, default="", blank=True, help_text="추가 요금")
    day  = models.CharField(max_length=500, default="", blank=True, help_text="일일 요금")
    detail  = models.CharField(max_length=500, default="", blank=True, help_text="요금 상세 정보")
    def __str__(self):
        charger = Charger.objects.filter(parking=self)
        if len(charger) !=0:
            return '%s' % (charger[0].name)
        return '%s' % ("새로운 주차 데이터")
    
    
class Charger(models.Model):
    INVISIBLE =(
    	('Y', 'Show'),
        ('N', 'Not show')
    )
    invisible = models.CharField(choices=INVISIBLE,max_length=3, default='Y', help_text="마커표시여부", blank=True)
    edit = models.BooleanField(default=False, help_text="수정한여부", blank=True)
    sid = models.CharField(max_length=500, help_text="충전소 아이디", primary_key=True)
    name = models.CharField(max_length=500, help_text="충전소 이름")
    ctype = models.ManyToManyField(Ctype)
    stat = models.PositiveIntegerField(default=0, help_text="충전소 상태") 
    addrDoro = models.CharField(max_length=500, help_text="도로명")
    addr2 = models.CharField(max_length=500, help_text="장소명", default="",blank=True)
    detail_addr = models.CharField(max_length=500, help_text="찾아가는 방법 설명", default="", blank=True)
    region1 = models.CharField(max_length=100, help_text="시도", default="")
    region2 = models.CharField(max_length=100, help_text="군구", default="")
    pay = models.ManyToManyField(Pay)
    bid = models.ManyToManyField(Bid)
    point = models.PointField(blank=True, null=True)
    lat = models.FloatField(max_length=100, default=0, help_text="위도",blank=True)
    lng = models.FloatField(max_length=100, default=0, help_text="경도",blank=True)
    tel = models.CharField(max_length=100, default="", help_text="고객센터 전화번호", blank=True)
    tel2 = models.CharField(max_length=100, default="", help_text="현장 전화번호", blank=True)
    LastUsedTime = models.CharField(max_length=20, help_text="마지막 사용 시간", blank=True)
    charger_img=models.ManyToManyField(ChargerImage, blank=True)
    charger_img1= models.ImageField(upload_to=get_upload_path_01, default="defalutChargerImg.jpg", blank=True)
    charger_img2= models.ImageField(upload_to=get_upload_path_02, default="defalutChargerImg.jpg", blank=True)
    charger_img3= models.ImageField(upload_to=get_upload_path_03, default="defalutChargerImg.jpg", blank=True)
    charger_img4= models.ImageField(upload_to=get_upload_path_04, default="defalutChargerImg.jpg", blank=True)

    charger_img01= models.ImageField(upload_to=get_upload_path_001, default="defalutChargerImg.jpg", blank=True)
    charger_img02= models.ImageField(upload_to=get_upload_path_002, default="defalutChargerImg.jpg", blank=True)
    charger_img03= models.ImageField(upload_to=get_upload_path_003, default="defalutChargerImg.jpg", blank=True)
    charger_img04= models.ImageField(upload_to=get_upload_path_004, default="defalutChargerImg.jpg", blank=True)

    note = models.CharField(max_length=300, default=0, help_text="노트", blank=True)
    parking = models.OneToOneField(ChargerParking, on_delete=models.CASCADE, related_name="charger", blank=True, null=True, default="")
    usetime = models.OneToOneField(Usetime, on_delete=models.CASCADE, related_name="charger", blank=True, null=True, default="")
    COLOR_CHOICES = (
        ('G', 'Green'),
        ('R', 'Red'),
        ('Y', 'Yellow')
    )

    n_c1 = models.CharField(max_length=1, 
        choices=COLOR_CHOICES, blank=True)
    n_t1 = models.CharField(max_length=100, blank=True)

    n_c2 = models.CharField(max_length=1, choices=COLOR_CHOICES, blank=True)
    n_t2 = models.CharField(max_length=100, blank=True)

    n_c3 = models.CharField(max_length=1, choices=COLOR_CHOICES, blank=True)
    n_t3 = models.CharField(max_length=100, blank=True)

    n_c4 = models.CharField(max_length=1, choices=COLOR_CHOICES, blank=True)
    n_t4 = models.CharField(max_length=100, blank=True)

    n_c5 = models.CharField(max_length=1, choices=COLOR_CHOICES, blank=True)
    n_c6 = models.CharField(max_length=100, blank=True)
    

    def __str__(self):
        return self.name
    @property
    def Count_avail(self):
        dic = {'slow_item':0, 'slow_avail':0, 'fast_item': 0, 'fast_avail': 0}
        charging = CidStat.objects.filter(sid=self.sid)
        for i in charging:
            ctype_list=[]
            for j in i.ctype.all():
                ctype_list.append(j.ctype)
            if 4 in ctype_list:
                dic['slow_item']=dic['slow_item'] + 1
                if i.stat == 2:
                    dic['slow_avail']=dic['slow_avail'] + 1
            else:
                dic['fast_item']=dic['fast_item'] + 1
                if  i.stat == 2:
                    dic['fast_avail']=dic['fast_avail'] + 1

        return dic
    @property
    def show(self):
        return self.invisible
        
    
    @property
    def bid_str(self):
        if len(self.bid.all()):
            return self.bid.all()[0].bid
        else:
            return "EC"
    @property
    def pay_str(self):
        if len(self.pay.all()):
            return self.pay.all()[0].get_pay_display
        else:
            return "EC"

    @property
    def show(self):
        return self.invisible
    @property
    def Charger_img_s(self):
        data=[]
        for i in self.charger_img.all():
            data.append("https://evonserver.s3.amazonaws.com/"+str(i.image))
        return data
        


class CidStat(models.Model):
    sidcid=models.CharField(max_length=100, default=0, help_text="고유충전기아이디", primary_key=True)
    sid = models.ForeignKey(Charger, on_delete=models.CASCADE, related_name="cidstat")
    cid = models.CharField(max_length=100, default=0, help_text="충전기 아이디")
    ctype = models.ManyToManyField(Ctype)
    stat = models.PositiveSmallIntegerField(default=0, help_text="충전기 아이디")
    def __str__(self):
        return '%s %s' % (self.sid, self.cid)
    @property
    def Cid(self):
        return int(self.cid)


class Region(models.Model):
    depth = models.PositiveSmallIntegerField(default=0, help_text="상세 주소 깊이")
    region = models.CharField(max_length=100, default=0, help_text="지역 주소", unique=True)
    lat = models.FloatField(max_length=100, default=0, help_text="x")
    lng = models.FloatField(max_length=100, default=0, help_text="y")
    def __str__(self):
        return '%s %s' % (self.depth, self.region)

class ChargerLog(models.Model):
    Charger = models.ForeignKey(Charger, on_delete=models.CASCADE, related_name="chargerlog")
    stat_before = models.PositiveSmallIntegerField(default=0, help_text="충전기 전 상태")
    stat_after = models.PositiveSmallIntegerField(default=0, help_text="충전기 바뀐 상태")
    time = models.DateTimeField(default=datetime.now, blank=True)
    def __str__(self):
        return '%s %s / (%s -> %s)' % (self.Charger, self.time, self.stat_before, self.stat_after)

class CidstatLog(models.Model):
    Cidstat = models.ForeignKey(CidStat, on_delete=models.CASCADE, related_name="chargerlog")
    stat_before = models.PositiveSmallIntegerField(default=0, help_text="충전기 전 상태")
    stat_after = models.PositiveSmallIntegerField(default=0, help_text="충전기 바뀐 상태")
    time = models.DateTimeField(default=datetime.now, blank=True)
    def __str__(self):
        return '%s %s / (%s -> %s)' % (self.Cidstat, self.time, self.stat_before, self.stat_after)

class VersionCheck(models.Model):
    version = models.CharField(max_length=100, default=0, help_text="버전", unique=True)
    time = models.DateTimeField(default=datetime.now, blank=True)
    require = models.BooleanField(help_text="필수체크", default=False, blank=True)
    note = models.CharField(max_length=300, default="", help_text="패치설명", blank=True)
    def __str__(self):
        return '%s' % (self.version)

class Viewtimes(models.Model):
    api_url = models.CharField(max_length=100, default=0, help_text="api종류", unique=True)
    views = models.PositiveIntegerField(default=0, help_text="조회수")
    def __str__(self):
        return '%s' % (self.api_url)

class ChargersNear(models.Model):

    CATEGORY_GROUP=(
        ('MT1', '대형마트'),
        ('CS2', '편의점'),
        ('PS3', '어린이집, 유치원'),
        ('SC4', '학교'),
        ('AC5', '학원'),
        ('PK6', '주차장'),
        ('OL7', '주유소, 충전소'),
        ('SW8', '지하철역'),
        ('BK9', '은행'),
        ('CT1', '문화시설'),
        ('AG2', '중개업소'),
        ('PO3', '공공기관'),
        ('AT4', '관광명소'),
        ('AD5', '숙박'),
        ('FD6', '음식점'),
        ('CE7', '카페'),
        ('HP8', '병원'),
        ('PM9', '약국'),
        ('ETC', '기타')
    )
    sid = models.ForeignKey(Charger, on_delete=models.CASCADE, related_name="chargernear")
    place_name = models.CharField(max_length=500, default=0, help_text="충전기 전 상태")
    lat = models.FloatField(max_length=100, default=0, help_text="x")
    lng = models.FloatField(max_length=100, default=0, help_text="y")
    address_name = models.CharField(max_length=500, default=0, help_text="충전기 바뀐 상태")
    road_address_name = models.CharField(max_length=500,blank=True)
    category_group_code = models.CharField(max_length=10, default="ETC", choices=CATEGORY_GROUP, help_text="카테고리")
    def __str__(self):
        return '%s-%s(%s)' % (self.sid, self.place_name, self.category_group_code)

