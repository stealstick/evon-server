from django.contrib import admin
from django.contrib.gis.db import models
from .models import Charger, CidStat, Region, Ctype, ChargerLog, CidstatLog, Pay, Bid, Usetime, VersionCheck, ChargersNear, ChargerParking, ChargerImage, Viewtimes
from mapwidgets.widgets import GooglePointFieldWidget
from django_extensions.admin import ForeignKeyAutocompleteAdmin

class ChargerAdmin(admin.ModelAdmin):
    search_fields = ['sid', 'name', 'addrDoro','bid__bid',]
    list_display = ('name','addrDoro')
    autocomplete_fields = ['parking', 'usetime']
    
    def chargerbid(self, obj):
        bid=list(obj.bid.all())
        return bid[0]

class CidstatLogAdmin(admin.ModelAdmin):
    search_fields = ['Cidstat__sid__sid', 'Cidstat__cid', 'Cidstat__sid__name', 'stat_before', 'stat_after','time']
    list_display = ('name', 'sid','cid', 'time','stat_before', 'stat_after')
    
    def sid(self, obj):
        sid=obj.Cidstat.sid.sid
        return sid
    def cid(self, obj):
        cid=obj.Cidstat.cid
        return cid
    def name(self, obj):
        name=obj.Cidstat.sid.name
        return name
class CidStatAdmin(admin.ModelAdmin):
    list_display = ('sid','sidcid')
    search_fields = ['sidcid', 'sid__name', 'sid__addrDoro', 'sid__bid__bid']
class VersioncheckAdmin(admin.ModelAdmin):
    list_display = ('version','time', 'require')
class RegionAdmin(admin.ModelAdmin):
    list_display = ('region','depth')
    search_fields = ['region']

class ParkingAdmin(admin.ModelAdmin):
    search_fields = ['sid']
class UsetimeAdmin(admin.ModelAdmin):
    search_fields = ['sid']

admin.site.register(Charger, ChargerAdmin)
admin.site.register(CidStat, CidStatAdmin)
admin.site.register(Region, RegionAdmin)
admin.site.register(Ctype)
admin.site.register(CidstatLog, CidstatLogAdmin)
admin.site.register(Pay)
admin.site.register(Bid)
admin.site.register(Usetime, UsetimeAdmin)
admin.site.register(ChargersNear)
admin.site.register(VersionCheck, VersioncheckAdmin)
admin.site.register(ChargerParking, ParkingAdmin)
admin.site.register(ChargerImage, )
admin.site.register(Viewtimes)